package vn.maiatech.king7.simulate;

import org.apache.commons.lang.math.IntRange;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by delta on 1/4/2016.
 */
public class RunManager {
    private static RunManager ourInstance = new RunManager();

    public static RunManager getInstance() {
        return ourInstance;
    }

    private int beginUserIdx;
    private int endUserIdx;
    private List<IntRange> availableUserIdxRanges;
    private List<RunnerBase> runners;

    private RunManager() {
        beginUserIdx = 0;
        endUserIdx = 0;
        availableUserIdxRanges = new LinkedList<>();
        runners = new LinkedList<>();
    }

    public void initialize(int beginUserIdx, int endUserIdx) {
        this.beginUserIdx = beginUserIdx;
        this.endUserIdx = endUserIdx;
        availableUserIdxRanges.clear();
        availableUserIdxRanges.add(new IntRange(beginUserIdx, endUserIdx));
        runners.clear();
    }

    public void addRunner(RunnerBase runner, int totalClients) {
        int beginIdx = -1;
        int endIdx = -1;
        synchronized (availableUserIdxRanges) {
            int minDelta = Integer.MAX_VALUE;
            int chosenIdx = -1;
            for (int i = 0; i < availableUserIdxRanges.size(); i++) {
                IntRange r = availableUserIdxRanges.get(i);
                int iDelta = Math.abs((r.getMaximumInteger() - r.getMinimumInteger() + 1) - totalClients);
                if (iDelta < minDelta) {
                    minDelta = iDelta;
                    chosenIdx = i;
                }
            }
            if (chosenIdx >= 0) {
                IntRange r = availableUserIdxRanges.get(chosenIdx);
                int l = r.getMinimumInteger();
                int h = r.getMaximumInteger();
                beginIdx = l;
                endIdx = (h - l + 1) > totalClients ? beginIdx + totalClients - 1 : h;
                // Update available range
                if (endIdx >= h)
                    availableUserIdxRanges.remove(chosenIdx);
                else
                    availableUserIdxRanges.set(chosenIdx, new IntRange(endIdx + 1, h));
            }
        }
        if (beginIdx >= 0) {
            synchronized (runners) {
                runners.add(runner);
            }
            // TODO: may use thread executor (if not as now, it is required runner.run to be async method)
            runner.run(beginIdx, endIdx);
        } else {
            System.out.println(String.format("No available bot for running %s", runner.getManagedId()));
        }
    }

    public void removeRunner(String runnerId) {
        synchronized (runners) {
            for (int i = 0; i < runners.size(); i++) {
                RunnerBase runner = runners.get(i);
                if (runnerId.equals(runner.getManagedId())) {
                    runner.stop();

                    // Free assigned user idx range
                    int low = runner.getAssignedUserIdxFrom();
                    int high = runner.getAssignedUserIdxTo();
                    boolean merged = false;
                    synchronized (availableUserIdxRanges) {
                        for (int j = 0; j < availableUserIdxRanges.size(); j++) {
                            IntRange r = availableUserIdxRanges.get(j);
                            int rLow = r.getMinimumInteger();
                            int rHigh = r.getMaximumInteger();
                            if (rLow == high + 1) {
                                availableUserIdxRanges.set(j, new IntRange(low, rHigh));
                                merged = true;
                                break;
                            } else if (low == rHigh + 1) {
                                availableUserIdxRanges.set(j, new IntRange(rLow, high));
                                merged = true;
                                break;
                            }
                        }
                        if (!merged)
                            availableUserIdxRanges.add(new IntRange(low, high));
                    }

                    runners.remove(i);
                    break;
                }
            }
        }
    }
}
