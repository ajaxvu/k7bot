package vn.maiatech.king7.simulate;

/**
 * Created by delta on 12/24/2015.
 */
public class WsMessage {
    public static int MC_SET_USER_IDX_RANGE = 0;
    public static int MC_LOGIN_SIMULATE = 1;
    public static int MC_GAME_STRESS_TEST = 2;
    public static int MC_GAME_AUTO_PLAY = 3;
    public static int MC_STOP_COMMAND = 128;

    public int code;
    public String id;
    public Object data;
}
