package vn.maiatech.king7.simulate;

/**
 * Created by delta on 1/7/2016.
 */
public class SessionData {
    public String userName;
    public String password;
    public String token;
    public Double cash;
    public boolean isRoomOwner;
    public String gameType;
}
