package vn.maiatech.king7.simulate;

/**
 * Created by delta on 3/16/2016.
 */
public enum ClientState {
    NotConnected,
    InitZoneConnecting, InitZoneConnected, InitZoneLoggingIn, InitZoneLoggedIn,
    GameZoneConnecting, GameZoneConnected, GameZoneLoggingIn, GameZoneLoggedIn,
    UserInfoGotten, TableGroupsGotten, TableCreating, GameJoining, GameJoint, GamePlaying
};
