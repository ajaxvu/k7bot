package vn.maiatech.king7.simulate;

import com.smartfoxserver.v2.exceptions.SFSException;
import sfs2x.client.SmartFox;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.User;

import java.util.Map;

/**
 * Created by delta on 12/28/2015.
 */
public abstract class GamePlayBase {
    protected  SmartFox sfs;
    protected SessionData sessionData;

    public final void enter(SmartFox sfs, SessionData sessionData, final ClientBase k7Client) {
        this.sfs = sfs;
        this.sessionData = sessionData;
        sfs.removeAllEventListeners();
        sfs.addEventListener(SFSEvent.USER_EXIT_ROOM, new IEventListener() {
            @Override
            public void dispatch(BaseEvent evt) throws SFSException {
                String type = evt.getType();
                Map<String, Object> params = evt.getArguments();
                if (type.equals(SFSEvent.USER_EXIT_ROOM)) {
                    User user = (User) params.get("user");
                    if(user == GamePlayBase.this.sfs.getMySelf()){
                        exit();
                        k7Client.backToLobby();
                    }
                }
            }
        });
        enter();
    }


    protected abstract void enter();

    protected abstract void exit();

}
