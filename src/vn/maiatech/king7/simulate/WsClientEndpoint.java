package vn.maiatech.king7.simulate;

/**
 * Created by delta on 12/22/2015.
 */

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

enum WsState {
    Connecting,
    Connected,
    Closing,
    Closed
}

@ClientEndpoint
public class WsClientEndpoint {
    Session userSession = null;
    private MessageHandler messageHandler;
    private final URI endpointURI;
    private final boolean autoReconnect;
    private final int pingInterval;
    private WsState state = WsState.Closed;
    private ScheduledFuture<?> reconnectRunner = null;
    private ScheduledFuture<?> pingRunner = null;
    private Object lock = new Object();

    public WsClientEndpoint(URI endpointURI, boolean autoReconnect, int pingInterval) {
        this.endpointURI = endpointURI;
        this.autoReconnect = autoReconnect;
        this.pingInterval = pingInterval;
        connect();
    }

    private void connect() {
        try {
            if (state == WsState.Closed) {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                synchronized (lock) {
                    state = WsState.Connecting;
                }
                container.connectToServer(this, endpointURI);
            }
        } catch (Exception e) {
            state = WsState.Closed;
            //throw new RuntimeException(e);
            System.err.println(String.format("WebSocket connection to %s failed with error: %s",
                    endpointURI.getHost(), e.getMessage()));
            retryRoutine();

        }
    }

    public void close() throws IOException {
        if (this.userSession != null) {
            synchronized (lock) {
                state = WsState.Closing;
            }
            this.userSession.close();
        }
    }

    private void retryRoutine() {
        if (autoReconnect && (reconnectRunner == null)) {
            reconnectRunner = new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            connect();
                        }
                    }, 0, 2000, TimeUnit.MILLISECONDS);
            System.out.println("Start WebSocket retrying routine " + endpointURI.getHost());
        }
    }

    private void pingRoutine() {
        if (pingInterval > 0 && state == WsState.Connected && pingRunner == null) {
            pingRunner = new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendMessage("ping");
                            }
                            catch(Exception e) {
                            }
                        }
                    },
                    0, pingInterval, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Callback hook for Connection open events.
     *
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
        synchronized (lock) {
            state = WsState.Connected;
        }
        System.out.println("WebSocket connected: " + endpointURI.getHost() + endpointURI.getPath());
        this.userSession = userSession;
        if (reconnectRunner != null) {
            reconnectRunner.cancel(false);
            reconnectRunner = null;
        }
        pingRoutine();
    }

    /**
     * Callback hook for Connection close events.
     *
     * @param userSession the userSession which is getting closed.
     * @param reason      the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
        System.out.println("WebSocket is closing...: " + endpointURI.getHost() + endpointURI.getPath());
        this.userSession = null;
        synchronized (lock) {
            state = WsState.Closed;
        }
        if(pingRunner != null){
            pingRunner.cancel(true);
            pingRunner = null;
        }
        retryRoutine();
    }

    /**
     * Callback hook for Message Events. This method will be invoked when a client send a message.
     *
     * @param message The text message
     */
    @OnMessage
    public void onMessage(String message) {
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(message);
        }
    }

    /**
     * register message handler
     *
     * @param
     */
    public void addMessageHandler(MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    /**
     * Send a message.
     *
     * @param
     * @param message
     */
    public void sendMessage(String message) {
        this.userSession.getAsyncRemote().sendText(message);
    }

    /**
     * Message handler.
     *
     * @author Jiji_Sasidharan
     */
    public static interface MessageHandler {

        public void handleMessage(String message);
    }
}