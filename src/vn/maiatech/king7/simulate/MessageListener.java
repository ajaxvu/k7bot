package vn.maiatech.king7.simulate;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Properties;

/**
 * Created by delta on 12/27/2015.
 */
public class MessageListener {
    public MessageListener() {
        try {
            String centerWsAddress = Config.getString("centerWsAddress");
            String[] playableGames = Config.getString("playableGames").split(",");
            final RunManager runManager = RunManager.getInstance();
            final Gson jsonParser = new Gson();
            String params = jsonParser.toJson(new WsRequestParams(playableGames));
            String wsURI = centerWsAddress + "?param=" + URLEncoder.encode(params, "UTF-8");
            WsClientEndpoint ws = new WsClientEndpoint(new URI(wsURI), true, 10000);
            ws.addMessageHandler(new WsClientEndpoint.MessageHandler() {
                @Override
                public void handleMessage(String message) {
                    System.out.println("Received message: " + message);
                    try {
                        WsMessage m = jsonParser.fromJson(message, WsMessage.class);
                        if (m.code == WsMessage.MC_SET_USER_IDX_RANGE) {
                            SetUserIdxRangeCmd cmd = jsonParser.fromJson(jsonParser.toJson(m.data), SetUserIdxRangeCmd.class);
                            runManager.initialize(cmd.beginIdx, cmd.endIdx);
                        } else if (m.code == WsMessage.MC_LOGIN_SIMULATE) {
                            LoginSimulateCmd cmd = jsonParser.fromJson(jsonParser.toJson(m.data), LoginSimulateCmd.class);
                            // TODO:
                        } else if (m.code == WsMessage.MC_GAME_STRESS_TEST) {
                            //TODO:
                            GameStressTestCmd cmd = jsonParser.fromJson(jsonParser.toJson(m.data), GameStressTestCmd.class);
                            String clientClassName = Config.getString(cmd.gameType + ".clientClassName");
                            if (clientClassName != null) {
                                //GameStressTestProcess.getInstance().run(cmd, Class.forName(clientClassName));
                                GameStressTestRunner runner = new GameStressTestRunner(m.id, cmd, Class.forName(clientClassName));
                                // TODO: hard-code for test one player
                                //cmd.ccu = 1;
                                runManager.addRunner(runner, cmd.ccu);
                            } else
                                System.err.println(String.format("Client of game type %s undefined", cmd.gameType));
                        }else if(m.code == WsMessage.MC_GAME_AUTO_PLAY){
                            GameAutoPlayCmd cmd = jsonParser.fromJson(jsonParser.toJson(m.data), GameAutoPlayCmd.class);
                            String clientClassName = Config.getString(cmd.gameType + ".clientClassName");
                            if (clientClassName != null) {
                                K7ZoneInfo initZoneInfo = new K7ZoneInfo();
                                initZoneInfo.host = Config.getString("entrance.host");
                                initZoneInfo.port = Config.getInt("entrance.port");
                                initZoneInfo.zoneName =  Config.getString("entrance.zone");
                                GameAutoPlayRunner runner = new GameAutoPlayRunner(m.id, cmd, initZoneInfo, Class.forName(clientClassName));
                                runManager.addRunner(runner, cmd.players);
                            }else
                                System.err.println(String.format("Client of game type %s undefined", cmd.gameType));
                        } else if(m.code == WsMessage.MC_STOP_COMMAND){
                            StopCommandCmd cmd = jsonParser.fromJson(jsonParser.toJson(m.data), StopCommandCmd.class);
                            runManager.removeRunner(cmd.cmdId);
                        } else {
                            throw new Exception("Undefined message code");
                        }
                    } catch (ClassNotFoundException e) {
                        System.err.println(String.format("Class %s not found", e.getMessage()));
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
            });

        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(e.hashCode());
        }
    }

    private class WsRequestParams {
        public String[] playableGames;

        public WsRequestParams(String[] playableGames) {
            this.playableGames = playableGames;
        }
    }
}
