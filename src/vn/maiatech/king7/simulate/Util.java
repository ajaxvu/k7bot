package vn.maiatech.king7.simulate;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by delta on 4/26/2016.
 */
public class Util {
    public static String calcMd5(String input){
        return DigestUtils.md5Hex(input).toUpperCase();
    }
}
