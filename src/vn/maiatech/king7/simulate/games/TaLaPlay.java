package vn.maiatech.king7.simulate.games;

import vn.maiatech.king7.simulate.GamePlayBase;

/**
 * Created by delta on 1/8/2016.
 */
public class TaLaPlay extends GamePlayBase {
    public void enter(){
        /**
         * Start your automation game here !!!
         * You can access SmartFox server instance by class member 'sfs', and user session data by class member 'sessionData'
         */
        System.out.println(String.format("%s: I am here %d", sessionData.userName, sfs.getJoinedRooms().get(0).getId()));
    }

    public void exit(){
        // Do something before leave game table
    }
}