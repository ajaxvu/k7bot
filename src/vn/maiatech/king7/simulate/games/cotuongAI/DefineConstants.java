package vn.maiatech.king7.simulate.games.cotuongAI;

public final class DefineConstants
{
	public static final int MAX_PLY_DEFAULT = 4;
	public static final int SIZE_X = 9;
	public static final int SIZE_Y = 10;
	public static final int MOVE_STACK = 4096;
	public static final int HIST_STACK = 1000;
	public static final int DARK = 0;
	public static final int LIGHT = 1;
	public static final int EMPTY = 7;
	public static final int PAWN = 0;
	public static final int BISHOP = 1;
	public static final int ELEPHAN = 2;
	public static final int KNIGHT = 3;
	public static final int CANNON = 4;
	public static final int ROOK = 5;
	public static final int KING = 6;
	public static final int INFINITY = 20000;
	public static final int NOMOVE = -1;
	public static final int HUMANTHINKING = 0;
	public static final int COMPUTERTHINKING = 1;
	public static final int COMPUTERMOVING = 2;
	public static final int GAMEOVER = 2;
	public static final int MAXREP = 30;
	public static final String BOOKFILENAME = "BOOK.DAT";
}