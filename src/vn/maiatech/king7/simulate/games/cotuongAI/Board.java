package vn.maiatech.king7.simulate.games.cotuongAI;

import java.util.ArrayList;
import java.util.List;

public class Board
{
	private int thinkDeepLevel = DefineConstants.MAX_PLY_DEFAULT;

	public Board()
	{
		thinkDeepLevel = DefineConstants.MAX_PLY_DEFAULT;
	}

	public Board(int thinkDeepLevel){
		this.thinkDeepLevel = thinkDeepLevel;
	}
	public void dispose()
	{
	}
	
	/**** DATA ****/
	public static byte[][][] pointtable =
			 	{{{0,  0,  0,  0,  0,  0,  0,  0,  0, 			/* PAWN*/
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					10,  0, 12,  0, 15,  0, 12,  0, 10,
					10,  0, 13,  0, 10,  0, 13,  0, 10,
					20, 20, 20, 20, 20, 20, 20, 20, 20,
					20, 21, 21, 22, 22, 22, 21, 21, 20,
					20, 21, 21, 23, 23, 23, 21, 21, 20,
					20, 21, 21, 23, 22, 23, 21, 21, 20,
					11, 12, 13, 14, 14, 14, 13, 12, 11},
	
				  {11, 12, 13, 14, 14, 14, 13, 12, 11,          /* PAWN*/
					20, 21, 21, 23, 22, 23, 21, 21, 20,
					20, 21, 21, 23, 23, 23, 21, 21, 20,
					20, 21, 21, 22, 22, 22, 21, 21, 20,
					20, 20, 20, 20, 20, 20, 20, 20, 20,
					10,  0, 13,  0, 10,  0, 13,  0, 10,
					10,  0, 12,  0, 15,  0, 12,  0, 10,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0}},

				{{0,  0,  0, 20,  0, 20,  0,  0,  0, 			/* BISHOP */
					0,  0,  0,  0, 22,  0,  0,  0,  0,
					0,  0,  0, 19,  0, 19,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0},

				   {0,  0,  0,  0,  0,  0,  0,  0,  0,          /* BISHOP */
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0, 19,  0, 19,  0,  0,  0,
					0,  0,  0,  0, 22,  0,  0,  0,  0,
					0,  0,  0, 20,  0, 20,  0,  0,  0}},

				  {{0,  0, 25,  0,  0,  0, 25,  0,  0, 			/* ELEPHAN */
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					23,  0,  0,  0, 28,  0,  0,  0, 23,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0, 22,  0,  0,  0, 22,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0},

				   {0,  0,  0,  0,  0,  0,  0,  0,  0,          /* ELEPHAN */
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0, 22,  0,  0,  0, 22,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
				   23,  0,  0,  0, 28,  0,  0,  0, 23,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0, 25,  0,  0,  0, 25,  0,  0}},

				  {{40, 35, 40, 40, 40, 40, 40, 35, 40, 		/* KNIGHT */
					40, 41, 42, 40, 20, 40, 42, 41, 40,
					40, 42, 43, 40, 40, 40, 43, 42, 40,
					40, 42, 43, 43, 43, 43, 43, 42, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 41, 42, 42, 42, 42, 42, 41, 40,
					40, 40, 40, 40, 40, 40, 40, 40, 40},

				   {40, 40, 40, 40, 40, 40, 40, 40, 40, 		/* KNIGHT */
					40, 41, 42, 42, 42, 42, 42, 41, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 43, 44, 44, 44, 44, 44, 43, 40,
					40, 42, 43, 43, 43, 43, 43, 42, 40,
					40, 42, 43, 40, 40, 40, 43, 42, 40,
					40, 41, 42, 40, 20, 40, 42, 41, 40,
					40, 35, 40, 40, 40, 40, 40, 35, 40}},

				  {{50, 50, 50, 50, 50, 50, 50, 50, 50, 		/* CANNON */
					50, 50, 50, 50, 50, 50, 50, 50, 50,
					50, 51, 53, 53, 55, 53, 53, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 51, 51, 51, 51, 51, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 50, 50, 50, 50, 50, 50, 50, 50},

				   {50, 50, 50, 50, 50, 50, 50, 50, 50, 		/* CANNON */
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 51, 51, 51, 51, 51, 51, 50,
					50, 51, 50, 50, 50, 50, 50, 51, 50,
					50, 51, 53, 53, 55, 53, 53, 51, 50,
					50, 50, 50, 50, 50, 50, 50, 50, 50,
					50, 50, 50, 50, 50, 50, 50, 50, 50}},


				  {{89, 92, 90, 90, 90, 90, 90, 92, 89, 		/* ROOK */
					91, 92, 90, 93, 90, 93, 90, 92, 91,
					90, 92, 90, 91, 90, 91, 90, 92, 90,
					90, 91, 90, 91, 90, 91, 90, 91, 90,
					90, 94, 90, 94, 90, 94, 90, 94, 90,
					90, 93, 90, 91, 90, 91, 90, 93, 90,
					90, 91, 90, 91, 90, 91, 90, 91, 90,
					90, 91, 90, 90, 90, 90, 90, 91, 90,
					90, 92, 91, 91, 90, 91, 91, 92, 90,
					90, 90, 90, 90, 90, 90, 90, 90, 90},

				   {90, 90, 90, 90, 90, 90, 90, 90, 90, 		/* ROOK */
					90, 92, 91, 91, 90, 91, 91, 92, 90,
					90, 91, 90, 90, 90, 90, 90, 91, 90,
					90, 91, 90, 91, 90, 91, 90, 91, 90,
					90, 93, 90, 91, 90, 91, 90, 93, 90,
					90, 94, 90, 94, 90, 94, 90, 94, 90,
					90, 91, 90, 91, 90, 91, 90, 91, 90,
					90, 92, 90, 91, 90, 91, 90, 92, 90,
					91, 92, 90, 93, 90, 93, 90, 92, 91,
					89, 92, 90, 90, 90, 90, 90, 92, 89}},

				  {{0,  0,  0, 30, 35, 30,  0,  0,  0, 			/* KING */
					0,  0,  0, 15, 15, 15,  0,  0,  0,
					0,  0,  0,  1,  1,  1,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0},

					{0,  0,  0,  0,  0,  0,  0,  0,  0,          /* KING */
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  0,  0,  0,  0,  0,  0,
					0,  0,  0,  1,  1,  1,  0,  0,  0,
					0,  0,  0, 15, 15, 15,  0,  0,  0,
					0,  0,  0, 30, 35, 30,  0,  0,  0}}};
	
	public static short[][] offset =
	{
		{-1, 1,13, 0, 0, 0, 0, 0},
		{-12,-14,12,14,0,0,0,0},
		{-28,-24,24,28, 0, 0, 0, 0},
		{-11,-15,-25,-27,11,15,25,27},
		{-1, 1,-13,13, 0, 0, 0, 0},
		{-1, 1,-13,13, 0, 0, 0, 0},
		{-1, 1,-13,13, 0, 0, 0, 0}
	}; // PAWN {for DARK side}

	public static short[] mailbox182 = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, -1,-1, 0, 1, 2, 3, 4, 5, 6, 7, 8,-1,-1, -1,-1, 9,10,11,12,13,14,15,16,17,-1,-1, -1,-1,18,19,20,21,22,23,24,25,26,-1,-1, -1,-1,27,28,29,30,31,32,33,34,35,-1,-1, -1,-1,36,37,38,39,40,41,42,43,44,-1,-1, -1,-1,45,46,47,48,49,50,51,52,53,-1,-1, -1,-1,54,55,56,57,58,59,60,61,62,-1,-1, -1,-1,63,64,65,66,67,68,69,70,71,-1,-1, -1,-1,72,73,74,75,76,77,78,79,80,-1,-1, -1,-1,81,82,83,84,85,86,87,88,89,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

	public static short[] mailbox90 = {28, 29, 30, 31, 32, 33, 34, 35, 36, 41, 42, 43, 44, 45, 46, 47, 48, 49, 54, 55, 56, 57, 58, 59, 60, 61, 62, 67, 68, 69, 70, 71, 72, 73, 74, 75, 80, 81, 82, 83, 84, 85, 86, 87, 88, 93, 94, 95, 96, 97, 98, 99,100,101, 106, 107,108,109,110,111,112,113,114, 119, 120,121,122,123,124,125,126,127, 132, 133,134,135,136,137,138,139,140, 145, 146,147,148,149,150,151,152,153};

	public static short[] legalposition = {1, 1, 5, 3, 3, 3, 5, 1, 1, 1, 1, 1, 3, 3, 3, 1, 1, 1, 5, 1, 1, 3, 7, 3, 1, 1, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 1,13, 1, 9, 1,13, 1, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9};

	public static short[] maskpiece = {8, 2, 4, 1, 1, 1, 2};
	public static short[] knightcheck = {1,-1,-9,-9,-1,1,9,9};
	public static short[] elephancheck = {-10,-8,8,10,0,0,0,0};
	public static short[][] kingpalace =
	{
		{3,4,5,12,13,14,21,22,23},
		{66,67,68,75,76,77,84,85,86}
	};

	public static short[] knightcheck2 = {-8,-10,-8,-10,8,10,8,10};

	public static boolean bStateBook = false;

	
	// For opening book 
	//private static boolean bReadBook = false;
	public static List<String> m_openingBook = new ArrayList<String>(); // need only one implement
	public List<List<String>> m_symmetricLine = new ArrayList<List<String>>();
	
	public void InitOpeningBook()
	{
		//Readbook();
		// init 4 list of string
		m_symmetricLine.add(new ArrayList<String>());
		m_symmetricLine.add(new ArrayList<String>());
		m_symmetricLine.add(new ArrayList<String>());
		m_symmetricLine.add(new ArrayList<String>());
	}
	/*
	public boolean Readbook()
	{
		if (bReadBook)
		{
			return bStateBook;
		}
    
		bReadBook = true; // one time only
		m_openingBook.clear();
    //TODO: ??? read file BOOK.DAT
		CFile f = new CFile();
		if (!f.Open(DefineConstants.BOOKFILENAME, CFile.modeRead))
		{
			return false;
		}
		CArchive ar = new CArchive(f, CArchive.load);
    
		String s;
		String line;
		while (ar.ReadString(s))
		{
			line = "";
			// Convert string into position
			for (int i = 0; i < s.length() && s.charAt(i) != ';'; i++)
			{
				if (s.charAt(i) >= 'A')
				{
	//C++ TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
	//ORIGINAL LINE: line += char(BYTE(s[i])-65 + (DefineConstants.SIZE_X-BYTE(s[i+1])+48)*DefineConstants.SIZE_X);
					line += (char)(byte)((byte)s.charAt(i) - 65 + (DefineConstants.SIZE_X - (byte)s.charAt(i + 1) + 48) * DefineConstants.SIZE_X);
					i++;
				}
			}
    
			if (line.length() > 0)
			{ // save in single linked list
				m_openingBook.AddTail(line);
				bStateBook = true;
			}
		}
		return bStateBook;
	}
	
	public int SymmetricConvert(int m, int k)
	{
		switch (k)
		{
			case 0:
				return m;
			case 1:
				return m + DefineConstants.SIZE_X - 1 - 2 * (m % DefineConstants.SIZE_X);
			case 2:
				return m + (DefineConstants.SIZE_Y - 1 - 2 * (m / DefineConstants.SIZE_X)) * DefineConstants.SIZE_X;
			case 3:
				return DefineConstants.SIZE_X * DefineConstants.SIZE_Y - 1 - m;
		}
		return m;
	}


	public MOVE GetOpeningBook()
	{
		int k; // sk: symmetrical kind
		int sk;
		boolean b = false;
		String line;
		MOVE move = new MOVE();
    
		move.from = DefineConstants.NOMOVE;
    
		// Computer go first
		if (m_hdp_interface == 0)
		{
			if (m_side_interface == DefineConstants.DARK)
			{
				sk = 2;
			}
			else
			{
				sk = 0;
			}
			k = 0;
			b = true;
		}
		else
		{
			if (m_hdp_interface * 2 == m_symmetricLine.get(0).size())
			{
				POSITION p = m_openingBook.GetHeadPosition();
    
				while (p != null)
				{
					line = m_openingBook.GetNext(p);
					for (sk = 0; sk < 4; sk++)
					{
						b = true;
						for (k = 0; k < m_hdp_interface * 2 && b != 0; k++)
						{
							if (m_symmetricLine[sk][k] != line.charAt(k) != null)
							{
								b = false;
							}
						}
						if (b != 0)
							break;
					}
					if (b != 0)
						break;
				}
			}
		}
    
		// Convert new move into normal
		if (b != 0)
		{
			move.from = SymmetricConvert(line.charAt(k), sk);
			move.dest = SymmetricConvert(line.charAt(k + 1), sk);
		}
    
		return move;
	}


	public boolean FollowOpeningBook()
	{
		if (!bStateBook)
		{
			return false;
		}
    
		m_newmove = GetOpeningBook();
		return (m_newmove.from != DefineConstants.NOMOVE);
	}
	
	public void UpdateNewMoveForFollowOpeningBook(short from, short dest)
	{
		if (bStateBook)
		{
			if (m_hdp_interface * 2 <= m_symmetricLine.get(0).size() + 2)
			{
				// Save 4 symmetrical forms
				int k = m_hdp_interface * 2;
				for (int i = 0; i < 4; i++)
				{
					if (k < m_symmetricLine.get(i).size())
					{
						m_symmetricLine.get(i).Left(k);
					}
					m_symmetricLine.get(i) += (byte)SymmetricConvert(from, i);
					m_symmetricLine.get(i) += (byte)SymmetricConvert(dest, i);
				}
			}
		}
	}*/

// For interface
	public final void ChangeSide()
	{
		short t = m_side_interface;
		m_side_interface = m_xside_interface;
		m_xside_interface = t;
	}
	public final void PreCalculate()
	{
		for (int i = 0; i < 90; i++)
		{
			m_piece[i] = m_piece_interface[i];
			m_color[i] = m_color_interface[i];
		}

		m_hdp = m_hdp_interface;
		m_side = m_side_interface;
		m_xside = m_xside_interface;
	}

	public void ComputerThink()
	{
		PreCalculate();

		//TODO: book???
//		if (FollowOpeningBook()) {
//			return;
//		}
    
		InitGen();
		SetMaterial();
    
		m_pv[0].from = DefineConstants.NOMOVE;
    
		m_newmove.from = DefineConstants.NOMOVE;
    
		for (short depth = 1; depth <= thinkDeepLevel; depth++)
		{
			m_bFollowPV = true;
			AlphaBeta(-DefineConstants.INFINITY, DefineConstants.INFINITY, depth);
			if (m_newmove.from == DefineConstants.NOMOVE) {
				break;
			}
		}
	}
	
	private byte[] InitData_color = {0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 7, 7, 7, 7, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 0, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 7, 7, 7, 7, 1, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	byte[] piece = {5, 3, 2, 1, 6, 1, 2, 3, 5,
					7, 7, 7, 7, 7, 7, 7, 7, 7,
					7, 4, 7, 7, 7, 7, 7, 4, 7,
					0, 7, 0, 7, 0, 7, 0, 7, 0,
					7, 7, 7, 7, 7, 7, 7, 7, 7,
					7, 7, 7, 7, 7, 7, 7, 7, 7,
					0, 7, 0, 7, 0, 7, 0, 7, 0,
					7, 4, 7, 7, 7, 7, 7, 4, 7,
					7, 7, 7, 7, 7, 7, 7, 7, 7,
					5, 3, 2, 1, 6, 1, 2, 3, 5};
	public final void InitData()
	{
		for (int i = 0; i < DefineConstants.SIZE_X * DefineConstants.SIZE_Y; i++)
		{
			m_piece_interface[i] = piece[i];
			m_color_interface[i] = InitData_color[i];
		}
		for (int i = 0; i < DefineConstants.HIST_STACK; i++) {
			m_hist_dat[i] = new HIST_REC();
		}
		for (int i = 0; i < DefineConstants.MOVE_STACK; i++) {
			m_gen_dat[i] = new GEN_REC();
		}
		for (int i = 0; i < DefineConstants.HIST_STACK; i++) {
			m_pv[i] = new MOVE();
		}
		m_side_interface = DefineConstants.LIGHT;
		m_xside_interface = DefineConstants.DARK;
		m_hdp_interface = 0;
		m_gameState = DefineConstants.HUMANTHINKING;
		InitOpeningBook();
	}
	public final boolean UpdateNewMove(int fromCol, int fromRow, int toCol, int toRow) {
		return UpdateNewMove(90 - (9 * fromRow + 9 - fromCol), 90 - (9 * toRow + 9 - toCol));
	}
	public final boolean UpdateNewMove(int from, int dest)
	{
		if (from == DefineConstants.NOMOVE || dest == DefineConstants.NOMOVE)
		{
			return true;
		}

		m_hist_dat[m_hdp_interface].m.from = from;
		m_hist_dat[m_hdp_interface].m.dest = dest;

		byte p = m_piece_interface[dest];
		m_hist_dat[m_hdp_interface].capture = p;

		m_piece_interface[dest] = m_piece_interface[from];
		m_piece_interface[from] = DefineConstants.EMPTY;
		m_color_interface[dest] = m_color_interface[from];
		m_color_interface[from] = DefineConstants.EMPTY;

		m_hdp_interface++;

		//UpdateNewMoveForFollowOpeningBook(from, dest); // TODO:
		return p == DefineConstants.KING;
	}
	public final boolean IsThereLegalMove()
	{
		PreCalculate();
		return Gen();
	}
	
	/*public final void Serialize(CArchive ar)
	{
		//TODO: ???
		int i;
		if (ar.IsStoring())
		{
			// saving
			ar << m_gameState;
			ar << m_side_interface;
			ar << m_xside_interface;
			ar << m_hdp_interface;
			for (i = 0; i < 90; i++)
			{
				ar << m_piece_interface[i];
				ar << m_color_interface[i];
			}

			for (i = 0; i < m_hdp_interface; i++)
			{
				ar << m_hist_dat[i].m.from;
				ar << m_hist_dat[i].m.dest;
				ar << m_hist_dat[i].capture;
			}
		}
		else
		{
			// loading
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
			ar >> m_gameState;
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
			ar >> m_side_interface;
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
			ar >> m_xside_interface;
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
			ar >> m_hdp_interface;
			for (i = 0; i < 90; i++)
			{
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
				ar >> m_piece_interface[i];
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
				ar >> m_color_interface[i];
			}

			for (i = 0; i < m_hdp_interface; i++)
			{
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
				ar >> m_hist_dat[i].m.from;
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
				ar >> m_hist_dat[i].m.dest;
//C++ TO JAVA CONVERTER WARNING: The right shift operator was not replaced by Java's logical right shift operator since the left operand was not confirmed to be of an unsigned type, but you should review whether the logical right shift operator (>>>) is more appropriate:
				ar >> m_hist_dat[i].capture;
			}

			InitOpeningBook();
		}
	}*/
	
	public final boolean CheckLegalMove(short from, short dest)
	{
		PreCalculate();
		InitGen();
		Gen();
		for (int i = m_gen_begin[0]; i < m_gen_end[0]; i++)
		{
			if (m_gen_dat[i].m.from == from && m_gen_dat[i].m.dest == dest)
			{
				return true;
			}
		}

		return false;
	}
	
	public final boolean UndoMove(RefObject<Integer> from, RefObject<Integer> dest)
	{
		if (m_hdp_interface == 0)
		{
			return false;
		}
		m_hdp_interface--;

		from.argValue = m_hist_dat[m_hdp_interface].m.from;
		dest.argValue = m_hist_dat[m_hdp_interface].m.dest;
		byte p = m_hist_dat[m_hdp_interface].capture;

		m_piece_interface[from.argValue] = m_piece_interface[dest.argValue];
		m_piece_interface[dest.argValue] = p;
		m_color_interface[from.argValue] = m_color_interface[dest.argValue];
		if (p == DefineConstants.EMPTY)
		{
			m_color_interface[dest.argValue] = DefineConstants.EMPTY;
		}
		else
		{
			m_color_interface[dest.argValue] = (byte)m_side_interface;
		}

		ChangeSide();
		return true;
	}
	public final int CanUndoMove()
	{
		return m_hdp_interface;
	}

	/* Data for interface 
	   We need duplicate data such as m_side, m_piece and so on
	   because while computer computes its move, the data will be
	   changed, but interface still needs original data for
	   display
	*/
	public short m_side_interface;
	public short m_xside_interface;
	public short m_hdp_interface;
	public byte[] m_piece_interface = new byte[DefineConstants.SIZE_X * DefineConstants.SIZE_Y];
	public byte[] m_color_interface = new byte[DefineConstants.SIZE_X * DefineConstants.SIZE_Y];
	public short m_gameState;
	public MOVE m_newmove = new MOVE();

// For calculation of move (Computer move)
	// data
	protected short m_side;
	protected short m_xside;
	protected short m_ply;
	protected short m_depth;
	protected byte[] m_piece = new byte[DefineConstants.SIZE_X * DefineConstants.SIZE_Y];
	protected byte[] m_color = new byte[DefineConstants.SIZE_X * DefineConstants.SIZE_Y];

	protected GEN_REC[] m_gen_dat = new GEN_REC[DefineConstants.MOVE_STACK];
	protected int[] m_gen_begin = new int[DefineConstants.HIST_STACK];
	protected int[] m_gen_end = new int[DefineConstants.HIST_STACK];
	protected HIST_REC[] m_hist_dat = new HIST_REC[DefineConstants.HIST_STACK];
	protected short m_hdp;
	protected short[][] m_history = new short[DefineConstants.SIZE_X * DefineConstants.SIZE_Y][DefineConstants.SIZE_X * DefineConstants.SIZE_Y];
	protected byte[][] m_materialnumber = new byte[2][7];
	protected MOVE[] m_pv = new MOVE[DefineConstants.HIST_STACK];
	protected boolean m_bFollowPV;

	// Gen functions
	public void InitGen()
	{
		m_gen_begin[0] = 0;
		m_ply = 0;
	}
	
	public boolean Attack(int pos, int side)
	{
		short j;
		short k;
		short x;
		short y;
		short fcannon;
		int sd;
		short xsd;
    
		sd = side;
		xsd = (short) (1 - sd);
    
		for (j = 0; j < 4; j++)
		{ // ROOK, CANNON, PAWN, KING
			x = mailbox90[pos];
			fcannon = 0;
			for (k = 0; k < 9; k++)
			{
				x = (short) (x + offset[DefineConstants.ROOK][j]);
				y = mailbox182[x];
				if (y == -1)
					break;
				if (fcannon == 0)
				{
					if (m_color[y] == xsd)
					{
						switch (m_piece[y])
						{
						case DefineConstants.ROOK:
							return true;
						case DefineConstants.KING:
							if (m_piece[pos] == DefineConstants.KING)
							{
								return true;
							}
						case DefineConstants.PAWN:
							if (k == 0 && ((sd == DefineConstants.DARK && j != 2) || (sd == DefineConstants.LIGHT && j != 3)))
							{
								return true;
							}
						}
					}
					if (m_color[y] != DefineConstants.EMPTY)
					{
						fcannon = 1;
					}
				}
				else // CANNON case
				{
					if (m_color[y] != DefineConstants.EMPTY)
					{
						if (m_color[y] == xsd && m_piece[y] == DefineConstants.CANNON)
						{
							return true;
						}
						break;
					}
				}
			} // for k
		} // for j
    
		for (j = 0; j < 8; j++)
		{ // Knight Check
			y = mailbox182[mailbox90[pos] + offset[DefineConstants.KNIGHT][j]];
			if (y == -1)
				continue;
			if (m_color[y] == xsd && m_piece[y] == DefineConstants.KNIGHT && m_color[pos + knightcheck2[j]] == DefineConstants.EMPTY)
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean IsInCheck(short side)
	{
		short i;
		short pos;
		i = 0;
		do
		{
			pos = kingpalace[side][i];
			i++;
		} while (m_piece[pos] != DefineConstants.KING);
		return Attack(pos, side);
	}

	public boolean CheckMoveLoop(short side)
	{
		short[] hdmap = new short[DefineConstants.MAXREP + 1];
		short c;
		int f;
		short i;
		short j;
		short k;
		short m;
		int p;
		boolean b;
		MOVE cm = new MOVE();
    
		/* if the side has not any attack pieces, it can make any move */
		if ((m_hdp < 4) || (m_materialnumber[side][DefineConstants.ROOK] + m_materialnumber[side][DefineConstants.CANNON] + m_materialnumber[side][DefineConstants.KNIGHT] + m_materialnumber[side][DefineConstants.PAWN] == 0))
		{
			return false;
		}
    
		for (i = 0; i < DefineConstants.MAXREP + 1; i++)
		{
			hdmap[i] = 0;
		}
		if (m_hdp > DefineConstants.MAXREP)
		{
			m = DefineConstants.MAXREP;
		}
		else
		{
			m = m_hdp;
		}
		c = 0;
		i = 0;
		k = 0;
    
		while (i < m)
		{
			// capture a piece - can not loop
			if (m_hist_dat[m_hdp - 1 - i].capture != DefineConstants.EMPTY)
			{
				return false;
			}
    
			if (hdmap[DefineConstants.MAXREP - i] == 0)
			{
				c++;
				hdmap[DefineConstants.MAXREP - i] = c; // track moves of a piece
				p = m_hist_dat[m_hdp - 1 - i].m.dest;
				f = m_hist_dat[m_hdp - 1 - i].m.from;
    
				j = (short) (i + 1);
				while (j < m)
				{
					if (f == m_hist_dat[m_hdp - 1 - j].m.dest)
					{
						f = m_hist_dat[m_hdp - 1 - j].m.from;
						hdmap[DefineConstants.MAXREP - j] = c;
						if (p == f)
						{
							if (k < j)
							{
								k = j;
							}
							break;
						}
					} // if (f
					j++;
				} // while (j<m)
    
				if (j >= m)
				{
					return false;
				}
			} // if (hdmap
			i++;
    
			// it is loop, but if the loop piece is 
			// attached and new place will 
			// help it to avoid attaching, let it moves
			if (i > 2 && i == k)
			{
				b = Attack(m_hist_dat[m_hdp - 1].m.dest, side);
				if (!b)
				{
					cm = m_hist_dat[m_hdp - 1].m;
					UnMakeMove();
					b = Attack(cm.from, side);
					MakeMove(cm);
					if (b)
					{
						return false;
					}
				}
				return true;
			}
		} // while (i<m)
		return false;
	}

	public boolean IsSafeMove(short from, short dest)
	{
		MOVE ms = new MOVE();
		boolean k;
    
		if (from == 88 && dest == 69)
		{
			k = true;
		}
		if (m_piece[dest] == DefineConstants.KING)
		{
			return true;
		}
		ms.from = from;
		ms.dest = dest;
		MakeMove(ms);
		k = IsInCheck(m_xside);
		if (!k)
		{
			k = CheckMoveLoop(m_xside);
		}
		UnMakeMove();
		return !k;
	}
	
	public void Gen_push(short from, short dest)
	{
		if (IsSafeMove(from, dest))
		{
			m_gen_dat[m_gen_end[m_ply]].m.from = from;
			m_gen_dat[m_gen_end[m_ply]].m.dest = dest;
			if (m_piece[dest] != DefineConstants.EMPTY)
			{
				 m_gen_dat[m_gen_end[m_ply]].prior = (short) ((m_piece[dest] + 1) * 100 - m_piece[from]);
			}
			else
			{
				m_gen_dat[m_gen_end[m_ply]].prior = m_history[from][dest];
			}
			m_gen_end[m_ply]++;
		}
	}
	
	public boolean Gen()
	{
		short i;
		short j;
		short k;
		short n;
		short p;
		short x;
		short y;
		short t;
		short fcannon;
    
		m_gen_end[m_ply] = m_gen_begin[m_ply];
    
		for (i = 0; i < DefineConstants.SIZE_X * DefineConstants.SIZE_Y; i++)
		{
			if (m_color[i] == m_side)
			{
				if (i == 84)
				{
					//i = i;
				}
				p = m_piece[i];
				for (j = 0; j < 8; j++)
				{
					if (offset[p][j] == 0)
						break;
					x = mailbox90[i];
					fcannon = 0;
					if (p == DefineConstants.ROOK || p == DefineConstants.CANNON)
					{
						n = 9;
					}
					else
					{
						n = 1;
					}
					for (k = 0; k < n; k++)
					{
						if (p == DefineConstants.PAWN && m_side == DefineConstants.LIGHT)
						{
							x -= offset[p][j];
						}
						else
						{
							x += offset[p][j];
						}
    
						y = mailbox182[x];
						if (m_side == DefineConstants.DARK)
						{
							t = y;
						}
						else
						{
							t = (short) (89 - y);
						}
						if (y == -1 || (legalposition[t] & maskpiece[p]) == 0)
							break;
						if (fcannon == 0)
						{
							if (m_color[y] != m_side)
							{
								switch (p)
								{
								case DefineConstants.KNIGHT:
									if (m_color[i + knightcheck[j]] == DefineConstants.EMPTY)
									{
										Gen_push(i, y);
									}
									break;
								case DefineConstants.ELEPHAN:
									if (m_color[i + elephancheck[j]] == DefineConstants.EMPTY)
									{
										Gen_push(i, y);
									}
									break;
								case DefineConstants.CANNON:
									if (m_color[y] == DefineConstants.EMPTY)
									{
										Gen_push(i, y);
									}
									break;
								default:
									Gen_push(i, y);
								}
							}
							if (m_color[y] != DefineConstants.EMPTY)
							{
								if (p == DefineConstants.CANNON)
								{
									fcannon++;
								}
								else
									break;
							}
						}
						else
						{ // CANNON switch
							if (m_color[y] != DefineConstants.EMPTY)
							{
								if (m_color[y] == m_xside)
								{
									Gen_push(i, y);
								}
								break;
							}
						}
					} // for k
				} // for j
			}
		}
    
		m_gen_end[m_ply + 1] = m_gen_end[m_ply];
		m_gen_begin[m_ply + 1] = m_gen_end[m_ply];
		return (m_gen_end[m_ply] > m_gen_begin[m_ply]);
	}
	
	public boolean GenCapture()
	{
		short i;
		short j;
		short k;
		short n;
		short p;
		short x;
		short y;
		short t;
		short fcannon;
    
		m_gen_end[m_ply] = m_gen_begin[m_ply];
    
		for (i = 0; i < DefineConstants.SIZE_X * DefineConstants.SIZE_Y; i++)
		{
			if (m_color[i] == m_side)
			{
				p = m_piece[i];
				for (j = 0; j < 8; j++)
				{
					if (offset[p][j] == 0)
						break;
					x = mailbox90[i];
					fcannon = 0;
    
					if (p == DefineConstants.ROOK || p == DefineConstants.CANNON)
					{
						n = 9;
					}
					else
					{
						n = 1;
					}
					for (k = 0; k < n; k++)
					{
						if (p == DefineConstants.PAWN && m_side == DefineConstants.LIGHT)
						{
							x -= offset[p][j];
						}
						else
						{
							x += offset[p][j];
						}
    
						y = mailbox182[x];
						if (m_side == DefineConstants.DARK)
						{
							t = y;
						}
						else
						{
							t = (short) (89 - y);
						}
						if (y == -1 || (legalposition[t] & maskpiece[p]) == 0)
							break;
						if (fcannon == 0)
						{
							if (m_color[y] != m_side)
							{
								switch (p)
								{
								case DefineConstants.KNIGHT:
									if (m_color[i + knightcheck[j]] == DefineConstants.EMPTY && m_color[y] == m_xside)
									{
										Gen_push(i, y);
									}
									break;
								case DefineConstants.ELEPHAN:
									if (m_color[i + elephancheck[j]] == DefineConstants.EMPTY && m_color[y] == m_xside)
									{
										Gen_push(i, y);
									}
									break;
								case DefineConstants.CANNON:
									break;
								default:
									if (m_color[y] == m_xside)
									{
										Gen_push(i, y);
									}
								}
							}
							if (m_color[y] != DefineConstants.EMPTY)
							{
								if (p == DefineConstants.CANNON)
								{
									fcannon = 1;
								}
								else
									break;
							}
						}
						else
						{ // CANNON switch
							if (m_color[y] != DefineConstants.EMPTY)
							{
								if (m_color[y] == m_xside)
								{
									Gen_push(i, y);
								}
								break;
							}
						}
					} // for k
				} // for j
			}
		}
		m_gen_end[m_ply + 1] = m_gen_end[m_ply];
		m_gen_begin[m_ply + 1] = m_gen_end[m_ply];
    
		return (m_gen_begin[m_ply] < m_gen_end[m_ply]);
	}

	public boolean MakeMove(MOVE m)
	{
		byte p;
		int from;
		int dest;
		short t;
    
		from = m.from;
		dest = m.dest;
		p = m_piece[dest];
		if (p != DefineConstants.EMPTY)
		{
			m_materialnumber[m_xside][p]--;
		}
		m_hist_dat[m_hdp].m = m;
		m_hist_dat[m_hdp].capture = p;
		m_piece[dest] = m_piece[from];
		m_piece[from] = DefineConstants.EMPTY;
		m_color[dest] = m_color[from];
		m_color[from] = DefineConstants.EMPTY;
		m_hdp++;
		m_ply++;
		t = m_side;
		m_side = m_xside;
		m_xside = t;
		return p == DefineConstants.KING;
	}
	
	public void UnMakeMove()
	{
		int from;
		int dest;
		short t;
		byte p;
    
		m_hdp--;
		m_ply--;
		t = m_side;
		m_side = m_xside;
		m_xside = t;
		from = m_hist_dat[m_hdp].m.from;
		dest = m_hist_dat[m_hdp].m.dest;
		p = m_hist_dat[m_hdp].capture;
		m_piece[from] = m_piece[dest];
		m_piece[dest] = p;
		m_color[from] = m_color[dest];
		m_color[dest] = DefineConstants.EMPTY;
		if (p != DefineConstants.EMPTY)
		{
			m_color[dest] = (byte)m_xside;
			m_materialnumber[m_xside][p]++;
		}
	}

	// Eval function
	public short Bonous()
	{
		short i;
		short s;
		short[][] bn =
		{
			{-2, -3, -3, -4, -4, -5, 0},
			{-2, -3, -3, -4, -4, -5, 0}
		};
    
		for (i = 0; i < 2; i++)
		{ // Scan DARK and LIGHT
			if (m_materialnumber[i][DefineConstants.BISHOP] < 2)
			{
				bn[1 - i][DefineConstants.ROOK] += 4;
				bn[1 - i][DefineConstants.KNIGHT] += 2;
				bn[1 - i][DefineConstants.PAWN] += 1;
			}
    
			if (m_materialnumber[i][DefineConstants.ELEPHAN] < 2)
			{
				bn[1 - i][DefineConstants.ROOK] += 2;
				bn[1 - i][DefineConstants.CANNON] += 2;
				bn[1 - i][DefineConstants.PAWN] += 1;
			}
		}
    
		if (m_color[0] == DefineConstants.DARK && m_color[1] == DefineConstants.DARK && m_piece[0] == DefineConstants.ROOK && m_piece[1] == DefineConstants.KNIGHT)
		{
			bn[DefineConstants.DARK][6] -= 10;
		}
		if (m_color[7] == DefineConstants.DARK && m_color[8] == DefineConstants.DARK && m_piece[8] == DefineConstants.ROOK && m_piece[7] == DefineConstants.KNIGHT)
		{
			bn[DefineConstants.DARK][6] -= 10;
		}
		if (m_color[81] == DefineConstants.LIGHT && m_color[82] == DefineConstants.LIGHT && m_piece[81] == DefineConstants.ROOK && m_piece[82] == DefineConstants.KNIGHT)
		{
			bn[DefineConstants.LIGHT][6] -= 10;
		}
		if (m_color[88] == DefineConstants.LIGHT && m_color[89] == DefineConstants.LIGHT && m_piece[89] == DefineConstants.ROOK && m_piece[88] == DefineConstants.KNIGHT)
		{
			bn[DefineConstants.LIGHT][6] -= 10;
		}
    
		s = (short) (bn[m_side][6] - bn[m_xside][6]);
    
		for (i = 0; i < 6; i++)
		{
			s += m_materialnumber[m_side][i] * bn[m_side][i] - m_materialnumber[m_xside][i] * bn[m_xside][i];
		}
		return s;
	}
	
	public short Eval()
	{
		short i;
		short s = 0;
		for (i = 0; i < DefineConstants.SIZE_X * DefineConstants.SIZE_Y; i++)
		{
			if (m_color[i] == DefineConstants.DARK)
			{
				s += pointtable[m_piece[i]][DefineConstants.DARK][i];
			}
			else if (m_color[i] == DefineConstants.LIGHT)
			{
				s -= pointtable[m_piece[i]][DefineConstants.LIGHT][i];
			}
		}
		if (m_side == DefineConstants.LIGHT)
		{
			s = (short) -s;
		}
		return (short) (s + Bonous());
	}

	// AlphaBeta function
	public int AlphaBeta(int alpha, int beta, int depth)
	{
		int i;
		int value;
		int best;
    
		if (depth == 0)
		{
			return Quiescence(alpha, beta);
		}
    
		Gen();
		best = -DefineConstants.INFINITY;
    
		for (i = m_gen_begin[m_ply]; i < m_gen_end[m_ply] && best < beta; i++)
		{
			if (best > alpha)
			{
				alpha = best;
			}
    
			if (MakeMove(m_gen_dat[i].m))
			{
				value = 1000 - m_ply;
			}
			else
			{
				value = -AlphaBeta(-beta, -alpha, depth - 1);
			}
			UnMakeMove();
    
			if (value > best)
			{
				best = value;
				if (m_ply == 0)
				{
					m_newmove = m_gen_dat[i].m;
				}
			}
		}
    
		return best;
	}

	protected final void SetMaterial()
	{
		int i;

		for (i = 0; i < 7; i++)
		{
			m_materialnumber[0][i] = 0;
			m_materialnumber[1][i] = 0;
		}

		for (i = 0; i < 90; i++)
		{
			if (m_piece[i] != DefineConstants.EMPTY)
			{
				m_materialnumber[m_color[i]][m_piece[i]]++;
			}
		}
	}

	public void Check_pv()
	{
		int i;
		for (m_bFollowPV = false, i = m_gen_begin[m_ply]; i < m_gen_end[m_ply]; i++)
		{
			if (m_gen_dat[i].m.from == m_pv[m_ply].from && m_gen_dat[i].m.dest == m_pv[m_ply].dest)
			{
				m_bFollowPV = true;
				m_gen_dat[i].prior += 1000;
				return;
			}
		}
	}
	
	public void Quicksort(int q, int r)
	{
		int i;
		int j;
		int x;
		GEN_REC g = new GEN_REC();
    
		i = q;
		j = r;
		x = m_gen_dat[(q + r) / 2].prior;
    
		do
		{
			while (m_gen_dat[i].prior > x)
			{
				i++;
			}
			while (m_gen_dat[j].prior < x)
			{
				j--;
			}
			if (i <= j)
			{
				g = m_gen_dat[i];
				m_gen_dat[i] = m_gen_dat[j];
				m_gen_dat[j] = g;
				i++;
				j--;
			}
		} while (i <= j);
    
		if (q < j)
		{
			Quicksort(q, j);
		}
		if (i < r)
		{
			Quicksort(i, r);
		}
	}
	
	public void Sort()
	{
		Quicksort(m_gen_begin[m_ply], m_gen_end[m_ply] - 1);
	}
	
	public int Quiescence(int alpha, int beta)
	{
		int i;
		int value;
		int best;
    
		value = Eval();
		if (value >= beta)
		{
			return value;
		}
		if (value > alpha)
		{
			alpha = value;
		}
    
		GenCapture();
		if (m_gen_begin[m_ply] >= m_gen_end[m_ply])
		{
			return value;
		}
		if (m_bFollowPV)
		{
			Check_pv();
		}
		Sort();
		best = -DefineConstants.INFINITY;
    
		for (i = m_gen_begin[m_ply];i < m_gen_end[m_ply] && best < beta; i++)
		{
			if (best > alpha)
			{
				alpha = best;
			}
    
			if (MakeMove(m_gen_dat[i].m))
			{
				value = (short) (1000 - m_ply);
			}
			else
			{
				value = -Quiescence(-beta, -alpha);
			}
			UnMakeMove();
    
			if (value > best)
			{
				best = value;
				m_pv[m_ply] = m_gen_dat[i].m;
			}
		}
    
		return best;
	}
	
}
