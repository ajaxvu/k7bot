package vn.maiatech.king7.simulate.games;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import vn.maiatech.king7.simulate.games.cotuongAI.Board;
import vn.maiatech.king7.simulate.games.cotuongAI.DefineConstants;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.User;
import sfs2x.client.requests.ExtensionRequest;
import vn.maiatech.king7.simulate.GamePlayBase;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import sfs2x.client.requests.LeaveRoomRequest;

/**
 * Created by delta on 1/8/2016.
 */


public class CoTuongPlay extends GamePlayBase {

    private enum GameState
    {
        CHO_THEM_NGUOI,
        CHO_CHOI,
        DANG_CHOI,
        KET_THUC
    }

    private enum Phe
    {
        KHONG_CO,
        DO,
        DEN
    }

    private enum eQuanCo
    {
        KHONG_CO(0, 7),
        TOT (1, 0),
        MA (2, 3),
        PHAO (3, 4),
        XE (4, 5),
        TINH (5, 2),
        SY (6, 1),
        TUONG (7, 6);
        int cId;
        int sId;
        eQuanCo(int clientId, int serverId) {
            this.cId = clientId;
            this.sId = serverId;
        }
    }
    private class Command {
        public static final String GAME_STATE = "GAME_STATE";
        public static final String GAME_WAIT_PLAYER = "GAME_WAIT_PLAYER";
        public static final String GAME_WAITING = "GAME_WAITING";
        public static final String GAME_START = "GAME_START";
        public static final String GAME_FINISH = "GAME_FINISH";
        public static final String FORCE_LEAVE = "FORCE_LEAVE";
        public static final String PLAYER_ENTER_ROOM = "PLAYER_ENTER_ROOM";
        public static final String PLAYER_READY = "PLAYER_READY";
        public static final String PLAYER_MOVE = "PLAYER_MOVE";
        public static final String PLAYER_XIN_HOA = "PLAYER_XIN_HOA";
        public static final String PLAYER_XIN_THUA = "PLAYER_XIN_THUA";
        public static final String PLAYER_DONG_Y_HOA = "PLAYER_DONG_Y_HOA";
        public static final String PLAYER_TU_CHOI_HOA = "PLAYER_TU_CHOI_HOA";
        public static final String PLAYER_EARN_EXP = "PLAYER_EARN_EXP";
        public static final String PLAYER_CHANGE_CASH = "PLAYER_CHANGE_CASH";
    }

    private class Param{
        public static final String RETURN_PARAM_STATUS = "status";
        public static final String RETURN_PARAM_MESSAGE = "message";
        public static final String RETURN_PARAM_USER_NAME = "userName";
        public static final String RETURN_PARAM_USER_AVATAR = "userAvatar";
        public static final String RETURN_PARAM_DISPLAY_NAME = "displayName";
        public static final String RETURN_PARAM_USER_SEX = "sex";
        public static final String RETURN_PARAM_VIP_ID = "vipID";
        public static final String RETURN_PARAM_USER_STAR = "star";
        public static final String RETURN_PARAM_EXP_EARN = "expEarn";
        public static final String RETURN_PARAM_IS_LEVEL_UP = "isLevelUp";
        public static final String RETURN_PARAM_LEVEL = "level";
        public static final String RETURN_PARAM_GAME_STATE = "gameState";
        public static final String RETURN_PARAM_TIME_WAITING = "timeWaiting";
        public static final String RETURN_PARAM_TIME_REMAIN = "timeRemain";
        public static final String RETURN_PARAM_TIME_REMAIN_DO = "timeRemainDo";
        public static final String RETURN_PARAM_TIME_REMAIN_DEN = "timeRemainDen";
        public static final String RETURN_PARAM_USER_AFFECT = "userAffect";
        public static final String RETURN_PARAM_USER_CASH = "userCash";
        public static final String RETURN_PARAM_USER_CASH_AFFECT = "userCashAffect";
        public static final String RETURN_PARAM_USER_MESSAGE_AFFECT = "userMessageAffect";
        public static final String RETURN_PARAM_QUICK_CHAT_LIST = "quickChatList";
        public static final String RETURN_PARAM_GAME_CASH = "gameCash";
        public static final String RETURN_PARAM_IS_READY = "isReady";
        public static final String RETURN_PARAM_PHE = "phe";
        public static final String RETURN_PARAM_CURRENT_PHE = "curPhe";
        public static final String RETURN_PARAM_PLAYER_LIST = "playerList";
        public static final String RETURN_PARAM_PLAYER_IN_GAME = "playerInGame";
        public static final String RETURN_PARAM_BAN_CO = "banCo";
        public static final String RETURN_PARAM_HANG = "hang";
        public static final String RETURN_PARAM_COT = "cot";
        public static final String RETURN_PARAM_TO_HANG = "toHang";
        public static final String RETURN_PARAM_TO_COT = "toCot";
        public static final String RETURN_PARAM_QUAN_CO = "quanCo";
        public static final String RETURN_PARAM_QUAN_CO_BI_AN = "quanCoBiAn";
        public static final String RETURN_PARAM_THU_TU = "thuTu";
        public static final String RETURN_PARAM_CHIEU_TUONG = "chieuTuong";
        public static final String RETURN_PARAM_SMS_NUMBER = "smsNumber";
        public static final String RETURN_PARAM_SMS_CONTENT = "smsContent";
    }

    // Board declaration
    private Board autoBoard;
    private Phe phe;
    private User myOpponent = null;


    public void enter() {
        /**
         * Start your automation game here !!!
         * You can access SmartFox server instance by class member 'sfs', and user session data by class member 'sessionData'
         */
        System.out.println(String.format("%s: I am here %d", sessionData.userName, sfs.getJoinedRooms().get(0).getId()));

        IEventListener evtListener = new GameEventHandler();
        sfs.addEventListener(SFSEvent.USER_ENTER_ROOM, evtListener);
        sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);
        sfs.addEventListener(SFSEvent.USER_EXIT_ROOM, evtListener);

        sfs.send(new ExtensionRequest(Command.PLAYER_ENTER_ROOM, new SFSObject(), sfs.getLastJoinedRoom()));
    }

    public void exit() {
        // Do something before leave game table
    }

    private class GameEventHandler implements IEventListener {
        @Override
        public void dispatch(BaseEvent evt) throws SFSException {
            if(evt == null){
                System.err.println("Unknown error: evt is null !!!");
                return;
            }
            String type = evt.getType();
            Map<String, Object> params = evt.getArguments();
            if (type.equals(SFSEvent.USER_ENTER_ROOM)) {
                User user = (User) params.get("user");
                System.out.println(String.format("%s: user %s just entered the room %d",
                        sessionData.userName, user.getName(), sfs.getJoinedRooms().get(0).getId()));
               /* if(myOpponent == null)
                    myOpponent = user;*/
            } else if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                if (params != null) {
                    SFSObject pars = (SFSObject) params.get("params");
                    String cmd = (String) params.get("cmd");
                    if (!pars.getBool(Param.RETURN_PARAM_STATUS)) {
                        // Error case
                        String msg = pars.getUtfString(Param.RETURN_PARAM_MESSAGE);
                        System.err.println(String.format("%s: Command %s responses  with error message: %s",
                                sessionData.userName, cmd, msg));
                    } else
                        processGameExtResponse(cmd, pars);
                }
            } else if (type.equals(SFSEvent.USER_EXIT_ROOM)) {
                User user = (User) params.get("user");
                if(user != sfs.getMySelf()){
                    System.out.println(String.format("%s: opponent %s left room %d",
                            sessionData.userName, user.getName(), sfs.getJoinedRooms().get(0).getId()));
                    sfs.send(new LeaveRoomRequest());
                }
            }
        }

        private void processGameExtResponse(String cmd, ISFSObject pms) {
            System.out.println(String.format("%s: extension response: %s. Table %d",
                    sessionData.userName, cmd, sfs.getJoinedRooms().get(0).getId()));
            if (cmd.equalsIgnoreCase(Command.GAME_STATE)) {
                int gameState = pms.getInt(Param.RETURN_PARAM_GAME_STATE);
                System.out.println(String.format("%s: table %d, game state %d",
                        sessionData.userName, sfs.getJoinedRooms().get(0).getId(), gameState));
            } else if (cmd.equalsIgnoreCase(Command.GAME_WAITING)) {
                // Also send ready
                ISFSObject pars = SFSObject.newInstance();
                pars.putBool(Param.RETURN_PARAM_IS_READY, true);
                sfs.send(new ExtensionRequest(Command.PLAYER_READY, pars, sfs.getLastJoinedRoom()));

            } else if (cmd.equalsIgnoreCase(Command.GAME_START)) {
                // Start game
                Random rn = new Random();
                int deepLevel = 3 + rn.nextInt(3); // deep is from 3 to 5 ^_^
                autoBoard = new Board(deepLevel);
                System.out.println(String.format("%s: start game with deep level %d", sessionData.userName, deepLevel));
                autoBoard.InitData();
                phe = Phe.values()[pms.getInt(Param.RETURN_PARAM_PHE)];
                if(phe == Phe.DO) {
                    // Bot di truoc
                    autoBoard.m_gameState = DefineConstants.COMPUTERTHINKING;
                    takeMoving();
                }
                else
                // Phia ben kia di truoc
                    autoBoard.m_gameState = DefineConstants.HUMANTHINKING;
            } else if (cmd.equalsIgnoreCase(Command.PLAYER_MOVE)) {
                // Update new move
                if(autoBoard.m_gameState == DefineConstants.HUMANTHINKING) {
                    autoBoard.m_gameState = DefineConstants.COMPUTERTHINKING;
                    int hang = pms.getInt(Param.RETURN_PARAM_HANG);
                    int cot = pms.getInt(Param.RETURN_PARAM_COT);
                    int toHang = pms.getInt(Param.RETURN_PARAM_TO_HANG);
                    int toCot = pms.getInt(Param.RETURN_PARAM_TO_COT);
                    System.out.println(String.format("Doi thu vua di: (%d,%d) -> (%d,%d)", hang, cot, toHang, toCot));
                    autoBoard.UpdateNewMove(cot, hang, toCot, toHang);
                    autoBoard.ChangeSide();
                    takeMoving();
                } else if(autoBoard.m_gameState == DefineConstants.COMPUTERTHINKING) {
                    autoBoard.m_gameState = DefineConstants.HUMANTHINKING;
                }
            } else if (cmd.equalsIgnoreCase(Command.GAME_FINISH)) {
                // TODO:
                System.out.println("Game finished !");
            }
        }

        private Random delayRnd = new Random();
        private void takeMoving(){
            final long MAX_THINK_TIME_ALLOW = 30000;
            Date beginTime = new Date();
            autoBoard.ComputerThink();
            Date endTime = new Date();
            long thinkSpent = endTime.getTime() - beginTime.getTime();
            if(thinkSpent < MAX_THINK_TIME_ALLOW - 8000){
                try {
                    //int maxDelay = (int)(MAX_THINK_TIME_ALLOW - 8000 - thinkSpent);
                    //int sleepTime = delayRnd.nextInt(maxDelay);
                    //Thread.sleep(sleepTime);
                    int sleepTime = 3000 + delayRnd.nextInt(2000);
                    Thread.sleep(sleepTime);
                } catch (InterruptedException ie) {
                    //Handle exception
                }
            }
            int z = autoBoard.m_newmove.from;
            int k = autoBoard.m_newmove.dest;
            boolean computerwon = false;
            // lưu nước cờ máy đi
            if(autoBoard.UpdateNewMove(z, k)) {
                computerwon = true;
            }
            autoBoard.ChangeSide();
            if (!autoBoard.IsThereLegalMove()) {
                computerwon = true;
            }
            if(computerwon) {
                System.out.println("Congraturation! Computer Won");
                autoBoard.m_gameState = DefineConstants.GAMEOVER;
            }

            // chuyển đổi vị trí xoay theo bàn cờ
            int fh = 9 - z/9;
            //int fh = z/9;
            int fc = z % 9;
            int th = 9 - k/9;
            //int th = k/9;
            int tc = k % 9;

            int tenqc = autoBoard.m_piece_interface[k];
            System.out.println(String.format("Hang: %d Cot: %d ", fh, fc));
            System.out.println("Quan co: " + tenqc);
            if (tenqc == eQuanCo.TUONG.sId) {
                tenqc = eQuanCo.TUONG.cId;
            } else if (tenqc == eQuanCo.SY.sId) {
                tenqc = eQuanCo.SY.cId;
            } else if (tenqc == eQuanCo.TINH.sId) {
                tenqc = eQuanCo.TINH.cId;
            } else if (tenqc == eQuanCo.XE.sId) {
                tenqc = eQuanCo.XE.cId;
            } else if (tenqc == eQuanCo.PHAO.sId) {
                tenqc = eQuanCo.PHAO.cId;
            } else if (tenqc == eQuanCo.MA.sId) {
                tenqc = eQuanCo.MA.cId;
            } else if (tenqc == eQuanCo.TOT.sId) {
                tenqc = eQuanCo.TOT.cId;
            } else {
                tenqc = eQuanCo.KHONG_CO.cId;
            }
            System.out.println("Quan co chuyen doi: " + tenqc);
                // Send request
            ISFSObject pars = SFSObject.newInstance();
            pars.putInt(Param.RETURN_PARAM_QUAN_CO, tenqc);
            pars.putInt(Param.RETURN_PARAM_HANG, fh);
            pars.putInt(Param.RETURN_PARAM_COT, fc);
            pars.putInt(Param.RETURN_PARAM_TO_HANG, th);
            pars.putInt(Param.RETURN_PARAM_TO_COT, tc);
            sfs.send(new ExtensionRequest(Command.PLAYER_MOVE, pars, sfs.getLastJoinedRoom()));
        }
    }
}