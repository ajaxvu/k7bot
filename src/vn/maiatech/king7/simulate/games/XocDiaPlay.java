package vn.maiatech.king7.simulate.games;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.User;
import sfs2x.client.requests.ExtensionRequest;
import vn.maiatech.king7.simulate.GamePlayBase;

import java.util.Map;
import java.util.Random;

/**
 * Created by delta on 12/28/2015.
 */
public class XocDiaPlay extends GamePlayBase {
    private class Command {
        public static final String GAME_STATE = "GAME_STATE";
        public static final String GET_USER_CAN_BET = "GET_USER_CAN_BET";
        public static final String GET_PLAYER_INFO = "GET_PLAYER_INFO";
        public static final String GET_PLAYER_BET_LIST = "GET_PLAYER_BET_LIST";
        public static final String GAME_WAITING = "GAME_WAITING";
        public static final String GAME_START = "GAME_START";
        public static final String GAME_PLAYING = "GAME_PLAYING";
        public static final String GAME_FINISH = "GAME_FINISH";
        public static final String PLAYER_READY = "PLAYER_READY";
        public static final String PLAYER_DAT_CUOC = "PLAYER_DAT_CUOC";
        public static final String PLAYER_HUY_CUOC = "PLAYER_HUY_CUOC";
        public static final String PLAYER_DAT_LAI_CUOC = "PLAYER_DAT_LAI_CUOC";
        public static final String PLAYER_DAT_GAP_DOI = "PLAYER_DAT_GAP_DOI";
        public static final String PLAYER_LAM_CAI = "PLAYER_LAM_CAI";
        public static final String PLAYER_HUY_LAM_CAI = "PLAYER_HUY_LAM_CAI";
        public static final String PLAYER_EARN_EXP = "PLAYER_EARN_EXP";
    }

    private class Param{
        public static final String RETURN_PARAM_STATUS = "status";
        public static final String RETURN_PARAM_MESSAGE = "message";
        public static final String RETURN_PARAM_USER_NAME = "userName";
        public static final String RETURN_PARAM_USER_AVATAR = "userAvatar";
        public static final String RETURN_PARAM_EXP_EARN = "expEarn";
        public static final String RETURN_PARAM_IS_LEVEL_UP = "isLevelUp";
        public static final String RETURN_PARAM_LEVEL = "level";
        public static final String RETURN_PARAM_PLAYER_ID = "playerID";
        public static final String RETURN_PARAM_GAME_STATE = "gameState";
        public static final String RETURN_PARAM_TIME_REMAIN = "timeRemain";
        public static final String RETURN_PARAM_USER_CASH = "userCash";
        public static final String RETURN_PARAM_USER_CASH_AFFECT = "userCashAffect";
        public static final String RETURN_PARAM_RESULT = "result";
        public static final String RETURN_PARAM_PLAYER_FINISH_CASH = "playerFinishCash";
        public static final String RETURN_PARAM_GAME_BET_RATE_LIST = "gameBetRateList";
        public static final String RETURN_PARAM_HISTORY_LIST = "historyList";
        public static final String RETURN_PARAM_CHAN_TOTAL = "chanTotal";
        public static final String RETURN_PARAM_LE_TOTAL = "leTotal";
        public static final String RETURN_PARAM_BET_CASH_LIST = "betCashList";
        public static final String RETURN_PARAM_KIEU_CUOC = "kieuCuoc";
        public static final String RETURN_PARAM_MUC_CUOC = "mucCuoc";
        public static final String RETURN_PARAM_BET_CASH = "betCash";
        public static final String RETURN_PARAM_PLAYER_BET_LIST = "playerBetList";
        public static final String RETURN_PARAM_ALL_PLAYER_BET_LIST = "allPlayerBetList";
        public static final String RETURN_PARAM_PLAYER_LAM_CAI_ID = "playerLamCaiID";
        public static final String RETURN_PARAM_QUICK_CHAT_LIST = "quickChatList";
    }


    public void enter(){
        /**
         * Start your automation game here !!!
         * You can access SmartFox server instance by class member 'sfs', and user session data by class member 'sessionData'
         */
        //System.out.println(String.format("%s: I am here %d", sessionData.userName, sfs.getJoinedRooms().get(0).getId()));

        IEventListener evtListener = new GameEventHandler();
        sfs.addEventListener(SFSEvent.USER_ENTER_ROOM, evtListener);
        sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);

        sfs.send(new ExtensionRequest(Command.PLAYER_READY, new SFSObject(), sfs.getLastJoinedRoom()));

    }

    public void exit(){
        // Do something before leave game table
    }

    private class GameEventHandler implements IEventListener{
        @Override
        public void dispatch(BaseEvent evt) throws SFSException {
            String type = evt.getType();
            Map<String, Object> params = evt.getArguments();
            if (type.equals(SFSEvent.USER_ENTER_ROOM)) {
                User user = (User)params.get("user");
                System.out.println(String.format("%s: user %s just entered the room %d",
                        sessionData.userName, user.getName(), sfs.getJoinedRooms().get(0).getId()));
            }
            else  if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                if (params != null) {
                    SFSObject pars = (SFSObject) params.get("params");
                    String cmd = (String) params.get("cmd");
                    if (!pars.getBool(Param.RETURN_PARAM_STATUS)) {
                        // Error case
                        String msg = pars.getUtfString(Param.RETURN_PARAM_MESSAGE);
                        System.err.println(String.format("%s: Command %s responses with error message: %s",
                                sessionData.userName, cmd, msg));
                    } else
                        processGameExtResponse(cmd, pars);
                }
            }
        }

        private void processGameExtResponse(String cmd, ISFSObject pms) {
            System.out.println(String.format("%s: extension response: %s. Table %d",
                    sessionData.userName, cmd, sfs.getJoinedRooms().get(0).getId()));
            if(cmd.equalsIgnoreCase(Command.GAME_STATE)){
                int gameState = pms.getInt(Param.RETURN_PARAM_GAME_STATE);
                System.out.println(String.format("%s: table %d, game state %d",
                        sessionData.userName, sfs.getJoinedRooms().get(0).getId(), gameState));
            } else if (cmd.equalsIgnoreCase(Command.GAME_PLAYING)){
                // Chon muc cuoc va dat cua ngau nhien
                Random rn = new Random();
                int betMoneyLevelType = rn.nextInt(3);  // I am a bot, so I don't all-in (level = 4) !
                int betType =  rn.nextInt(5);
                ISFSObject pars = SFSObject.newInstance();
                pars.putInt(Param.RETURN_PARAM_KIEU_CUOC, betType);
                pars.putInt(Param.RETURN_PARAM_MUC_CUOC, betMoneyLevelType);
                sfs.send(new ExtensionRequest(Command.PLAYER_DAT_CUOC, pars, sfs.getLastJoinedRoom()));
            } else if(cmd.equalsIgnoreCase(Command.PLAYER_DAT_CUOC)){
                // TODO
            }
            else if (cmd.equalsIgnoreCase(Command.GAME_FINISH)) {
                // TODO:
                //System.out.println("Game finished !");
            }
        }
    }
}
