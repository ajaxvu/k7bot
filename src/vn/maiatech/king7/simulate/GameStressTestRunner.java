package vn.maiatech.king7.simulate;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import sfs2x.client.SmartFox;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;
import sfs2x.client.requests.LoginRequest;
import sfs2x.client.util.ConfigData;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by delta on 1/5/2016.
 */

public class GameStressTestRunner extends RunnerBase {
    private final GameStressTestCmd testCmd;
    private final Class<?> clientClass;

    private int checkedClientIdx;

    private final List<K7GameClient> clients;
    private final int toBeCreatedTables;
    //private int creatingTables;
    //private int createdTables;
    private final Object createdTableLock = new Object();

    private ScheduledFuture<?> generationTask;

    public GameStressTestRunner(String managedId, GameStressTestCmd cmd, final Class<?> clientClass) {
        id = managedId;
        testCmd = cmd;
        this.clientClass = clientClass;
        clients = new LinkedList<>();
        toBeCreatedTables = cmd.toBeCreatedRooms;
        //createdTables = 0;
        //creatingTables = 0;
    }

    protected void run() {
        int createdTables = 0;
        for (int i = assignedUserIdxFrom; i <= assignedUserIdxTo; i++) {
            boolean isTableOwner = false;
            if(createdTables < toBeCreatedTables){
                isTableOwner = true;
                createdTables++;
            }

            BotGenerator.BotLoginInfo loginInfo = BotGenerator.getInstance().generate(i);
            K7GameClient client = new K7GameClient(loginInfo.userName, loginInfo.password,
                    testCmd.entranceServerInfo, testCmd.gameType, clientClass, isTableOwner);
            clients.add(client);
        }

        checkedClientIdx = 0;
        generationTask = new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        K7GameClient client = clients.get(checkedClientIdx);

                        checkedClientIdx++;
                        if (checkedClientIdx >= clients.size())
                            checkedClientIdx = 0;

                        if (client.getState() == ClientState.NotConnected)
                            client.login();
                       else if(client.getState() == ClientState.UserInfoGotten)
                            client.getRoomsInfo();
                    }
                }
                , 0, 5000, TimeUnit.MILLISECONDS);
    }

    public void doStop() {
        generationTask.cancel(true);
        System.out.println("Stopping the runner " + id);
        for (K7GameClient client : clients) {
            client.disconnect();
        }
    }

    public class K7GameClient {
        private SmartFox sfs = null;
        private ConfigData cfg;
        private final String userName;
        private final String password;
        private String userToken;
        private double userCash;
        private final K7ZoneInfo entranceServerInfo;
        private final String initZone;
        private final String gameType;
        private  String gameZone;
        final Class<?> clientClass;
        private ClientState state;
        private String selectedRoomGroupId; //Note: this is the bad design of the game extension !
        private final boolean isTableOwner;
        private IEventListener lobbyEventListener;


        public K7GameClient(String userName, String password, K7ZoneInfo entranceServerInfo, String gameType,
                            Class<?> clientClass, boolean isTableOwner) {
            this.userName = userName;
            this.password = password;
            this.entranceServerInfo = entranceServerInfo;
            this.initZone = entranceServerInfo.zoneName == null ? "InitZone" : entranceServerInfo.zoneName;
            this.gameType = gameType;
            this.gameZone = gameType;   //TODO: may be changed later!
            this.clientClass = clientClass;
            this.isTableOwner = isTableOwner;
            state = ClientState.NotConnected;
        }

        public ClientState getState() {
            return state;
        }

        public void disconnect() {
            if (sfs != null && sfs.isConnected()) {
                state = ClientState.NotConnected;
                sfs.disconnect();
            }
        }

        private void getUserInfo() {
            ISFSObject pars = SFSObject.newInstance();
            pars.putUtfString("token", userToken);
            sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_USER_INFO, pars));
        }

        /**
         * Login to K7 server (initial zone)
         */
        public void login() {
            sfs = new SmartFox();
            cfg = new ConfigData();

            IEventListener evtListener = new K7InitZoneEvtHandler();

            cfg.setHost(entranceServerInfo.host);
            cfg.setPort(entranceServerInfo.port);
            cfg.setZone(initZone);
            cfg.setUseBBox(false);

            sfs.addEventListener(SFSEvent.CONNECTION, evtListener);
            sfs.addEventListener(SFSEvent.CONNECTION_LOST, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN_ERROR, evtListener);
            sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);
            sfs.connect(cfg);
            state = ClientState.InitZoneConnecting;
        }

        private class K7InitZoneEvtHandler implements IEventListener{
            @Override
            public void dispatch(BaseEvent evt) throws SFSException {
                String type = evt.getType();
                Map<String, Object> params = evt.getArguments();

                if (type.equals(SFSEvent.CONNECTION)) {
                    boolean success = (Boolean) params.get("success");
                    if (success) {
                        state = ClientState.InitZoneConnected;
                        ISFSObject pars = SFSObject.newInstance();
                        pars.putUtfString("type", "login");
                        pars.putUtfString("pass", password);
                        sfs.send(new LoginRequest(userName, password, initZone, pars));
                        state = ClientState.InitZoneLoggingIn;
                    } else {
                        state = ClientState.NotConnected;
                        System.err.println(userName + ": connection to K7 server failed");
                        // TODO: Dispatch event...
                    }
                } else if (type.equals(SFSEvent.CONNECTION_LOST)) {
                    state = ClientState.NotConnected;
                    System.out.println(userName + ": client disconnected from K7 server. ");
                    // TODO: Dispatch event...
                } else if (type.equals(SFSEvent.LOGIN)) {
                    state = ClientState.InitZoneLoggedIn;
                    System.out.println(userName + ": login to K7 server OK");
                    // Retrieve token from server
                    ISFSObject pms = (ISFSObject) params.get("data");
                    String token = pms.getUtfString("token");
                    //System.out.println("Token " + token);
                    userToken = token;
                    // Send request to get list of games
                    //ISFSObject pars = SFSObject.newInstance();
                    //pars.putUtfString("token", userToken);
                    //sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_GAME_TYPE_DATA, pars));
                    getUserInfo();  // TODO: simulate the shit logic of client !!!
                    // TODO: Dispatch event...7
                } else if (type.equals(SFSEvent.LOGIN_ERROR)) {
                    state = ClientState.NotConnected;
                    System.out.println(userName + ": login failure: " + (String)params.get("errorMessage"));
                        /*short errCode = (short) params.get("errorCode");
                        // If not the case of "The server is full" error, we remove client from list
                        if (errCode != 7) {
                        }*/
                    // TODO: Dispatch event...
                } else if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                    String cmd = (String) params.get("cmd");
                    ISFSObject pms = (ISFSObject) params.get("params");
                    if(cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_USER_INFO)){
                        if(pms.getBool("status")){
                            userCash = sfs.getMySelf().getVariable("UserCash").getDoubleValue();
                            // Send request to get list of games
                            ISFSObject pars = SFSObject.newInstance();
                            //pars.putUtfString("token", userToken);
                            sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_GAME_TYPE_DATA, pars));
                        }
                        else{
                            String message = pms.getUtfString ("message");
                            System.err.println(userName + ": get user info responses with error: " + message);
                        }
                    } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_GAME_TYPE_DATA)) {
                        Collection<String> gameTypes = pms.getUtfStringArray("gameTypeID");
                        //Collection<String> gameZones = pms.getUtfStringArray("gameTypeName");
                        // Login to game zone
                        if (gameTypes.contains(gameType)) {
                            sfs.removeAllEventListeners();
                            sfs.disconnect();
                            // TODO: get Host, Port from server
                            loginToGameZone(entranceServerInfo.host, entranceServerInfo.port);
                        } else {
                            System.err.println(String.format("Game type %s not supported", gameType));
                        }
                    }
                }
            }
        }

        /**
         * Login to selected game zone
         * @param host
         * @param port
         */
        private void loginToGameZone(String host, int port) {
            sfs = new SmartFox();
            cfg = new ConfigData();
            cfg.setHost(host);
            cfg.setPort(port);
            cfg.setZone(gameZone);
            cfg.setUseBBox(false);

            IEventListener evtListener = new K7LobbyEvtHandler();

            lobbyEventListener = evtListener;

            sfs.addEventListener(SFSEvent.CONNECTION, evtListener);
            sfs.addEventListener(SFSEvent.CONNECTION_LOST, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN_ERROR, evtListener);
            sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);

            sfs.connect(cfg);

            state = ClientState.GameZoneConnecting;
        }

        private class K7LobbyEvtHandler implements IEventListener{
            @Override
            public void dispatch(BaseEvent evt) throws SFSException {
                String type = evt.getType();
                Map<String, Object> params = evt.getArguments();
                if (type.equals(SFSEvent.CONNECTION)) {
                    boolean success = (Boolean) params.get("success");
                    if (success) {
                        state = ClientState.GameZoneConnected;
                        sfs.send(new LoginRequest(userName, password));
                        state = ClientState.GameZoneLoggingIn;
                    } else {
                        state = ClientState.NotConnected;
                        System.err.println(String.format("%s: connection to game zone %s failure", userName, gameZone));
                    }
                } else if (type.equals(SFSEvent.LOGIN)) {
                    state = ClientState.GameZoneLoggedIn;
                    System.out.println(userName + ": login to game zone " + gameZone + " success");
                    getUserInfo();  // TODO: will remove when try a better solution for game extension

                } else if (type.equals(SFSEvent.CONNECTION_LOST)) {
                    state = ClientState.NotConnected;
                    System.out.println(userName + ": connection to game zone " + gameZone + " lost");
                   /* if(isTableOwner){
                        synchronized (createdTableLock) {
                            createdTables--;
                        }
                        isTableOwner = false;
                    }*/
                } else if (type.equals(SFSEvent.LOGIN_ERROR)) {
                    state = ClientState.GameZoneConnected;
                    //TODO:
                    System.out.println(userName + ": login to game zone " + gameZone + " error: " +
                            (String)params.get("errorMessage"));
                } else if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                    processGameExtResponse((String) params.get("cmd"), (SFSObject) params.get("params"));
                }
            }

            private void processGameExtResponse(String cmd, ISFSObject pms) {
                if(cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_USER_INFO)){
                    state = ClientState.UserInfoGotten;
                    // Send command to get information about rooms, tables,...
                    getRoomsInfo();
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_ROOM_TYPE_DATA)) {
                    if (pms.getBool("status")) {
                        Collection<String> roomGroupIds = pms.getUtfStringArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_TYPE_ID);
                        //Collection<String> roomGroupNames = pms.getUtfStringArray(SFS_PARAM_TABLE_LIST_ROOM_TYPE_NAME);
                        if (isTableOwner && state == ClientState.UserInfoGotten) {
                            // Try to create new table
                            int selectedIdx = ThreadLocalRandom.current().nextInt(0, roomGroupIds.size() - 1);
                            List<String> lstRoomGroupIds = new ArrayList(roomGroupIds);
                            selectedRoomGroupId = lstRoomGroupIds.get(selectedIdx);
                            ISFSObject pars = SFSObject.newInstance();
                            pars.putUtfString("zonename", gameZone);
                            pars.putUtfString("groupid", selectedRoomGroupId);
                            pars.putUtfString("token", userToken);
                            state = ClientState.TableCreating;
                            sfs.send(new ExtensionRequest(K7Def.SFS_CMD_CREATE_TABLE_PREPARE, pars));
                        } else {
                           /* synchronized (createdTableLock) {
                                creatingTables--;
                            }*/
                            // Try to join a table
                            List<Room> tables = new LinkedList<>();
                            for (String roomGroupId : roomGroupIds) {
                                List<Room> rooms = sfs.getRoomListFromGroup(roomGroupId);
                                for (Room room : rooms) {
                                    //System.out.println(String.format("Found room %s(%d)", room.getName(), room.getUserCount()));
                                    tables.add(room);
                                }
                            }

                            Collections.shuffle(tables);
                            if(tables.size() > 0) {
                                Room selectedTable = tables.get(0);
                                System.out.println(String.format("Table: %d, user: %d", selectedTable.getId(), selectedTable.getUserCount()));
                                if(selectedTable.getUserCount() > 0 && selectedTable.getUserCount()< testCmd.maxPlayersInGame)    // Don't enter the table before table owner !!!
                                    enterGame(selectedTable.getId());
                            }
                           /* boolean availableRoomFound = false;
                            for (Room table : tables) {
                                System.out.println(String.format("%s: sees table %d: %d", userName, table.getId(), table.getVariable("totalUser").getIntValue()));
                                //TODO: change here
                                //if (table.getMaxUsers() > table.getVariable("totalUser").getIntValue()){
                                if (table.getUserCount() < 9) {
                                    //if (table.getMaxUsers() > table.getVariable("totalUser").getIntValue()) {
                                    //if (table.getMaxUsers() > 1) {
                                    availableRoomFound = true;
                                    enterGame(table.getId());
                                    break;
                                }
                            }
                            if (!availableRoomFound)
                                System.out.println(String.format("%s: no available room", userName));*/
                        }

                    } else {
                        System.err.println(String.format(userName + ": get room data error: %s", pms.getUtfString("message")));
                    }

                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_CREATE_TABLE_PREPARE)) {
                    if (pms.getBool("status")) {
                        onReceivedTableGroupInfo(pms);
                    } else {
                        state = ClientState.UserInfoGotten;
                        System.err.println(String.format(userName + ": table creating preparation error: %s",
                                pms.getUtfString("message")));
                    }
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_CREATE_TABLE)) {
                    if (pms.getBool("status")) {
                        /*synchronized (createdTableLock) {
                            createdTables++;
                            creatingTables--;
                        }*/
                        int tableId = pms.getInt("roomID");
                        System.out.println(String.format("%s created table %d", userName, tableId));
                        //isTableOwner = true;
                        enterGame(tableId);
                    } else {
                        /*synchronized (createdTableLock) {
                            creatingTables--;
                        }*/
                        state = ClientState.UserInfoGotten;
                        System.err.println(String.format("%s: table creating error: %s", userName, pms.getUtfString("message")));
                    }
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_USER_JOIN_ROOM)) {
                    if (pms.getBool("status")) {
                        if (pms.containsKey("islobby") && pms.getBool("islobby")) {
                            System.out.println(userName + ": joined to lobby room success");
                        } else {
                            state = ClientState.GameJoint;
                            System.out.println(String.format("%s: joined to game table %d success",
                                    userName, sfs.getJoinedRooms().get(0).getId()));
                            // Create game client
                            try {
                                GamePlayBase gc = (GamePlayBase) clientClass.newInstance();
                                SessionData sd = new SessionData();
                                sd.userName = userName;
                                sd.password = password;
                                sd.token = userToken;
                                sd.cash = userCash;
                                sd.gameType = gameType;
                                sd.isRoomOwner = isTableOwner;
                                //gc.enter(sfs, sd, K7GameClient.this);
                            }
                            catch(Exception e){
                                System.err.println(String.format("%s: error when creating game client %s: %s",
                                        userName, clientClass.toString(), e.getMessage()));
                            }
                        }
                    } else {
                        state = ClientState.UserInfoGotten;
                        System.err.println(String.format(userName + ": join game error: %s", pms.getUtfString("message")));
                    }
                }
            }

            private void onReceivedTableGroupInfo(ISFSObject pms) {
                String tableDefaultName = pms.getUtfString(K7Def.SFS_PARAM_TABLE_LIST_ROOM_NAME);
                int maxUser = pms.getInt(K7Def.SFS_PARAM_TABLE_LIST_ROOM_MAX_USER);
                Collection<String> tableExtNames = pms.getUtfStringArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_EXT_NAME);
                Collection<String> tableExtKeys = pms.getUtfStringArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_EXT_KEY);
                List<Double> tableBets = new ArrayList(pms.getDoubleArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_MONEY_CASH));

                ISFSObject pars = SFSObject.newInstance();
                pars.putUtfString("roomname", tableDefaultName + "_" + userName);
                pars.putUtfString("groupid", selectedRoomGroupId);
                pars.putDouble("roomcash", tableBets.size() > 0 ? tableBets.get(0) : 0); // Always choose the first defined bet
                pars.putInt("maxuser", maxUser);
                pars.putUtfString("zonename", gameZone);
                pars.putUtfString("password", "");

                if (tableExtKeys.size() > 0) {
                    Collection<Boolean> arrExtVal = Arrays.asList(new Boolean[]{true, false});  // TODO: may remove the hard code
                    pars.putBoolArray("extval", arrExtVal);
                    pars.putUtfStringArray("extkey", tableExtKeys);
                }

                sfs.send(new ExtensionRequest(K7Def.SFS_CMD_CREATE_TABLE, pars));
            }

            private void enterGame(int tableId) {
                state = ClientState.GameJoining;
                ISFSObject pars = SFSObject.newInstance();
                pars.putUtfString("zonename", gameZone);
                pars.putInt("roomid", tableId);
                //pars.putUtfString("userID", us);
                System.out.println(String.format("%s: Try to join table: %d", userName, tableId));
                sfs.send(new ExtensionRequest(K7Def.SFS_CMD_USER_JOIN_ROOM, pars));
            }
        }

        public void getRoomsInfo(){
            ISFSObject pars = SFSObject.newInstance();
            pars.putUtfString("zonename", gameZone);
            /*synchronized (createdTableLock) {
                if(createdTables + creatingTables < toBeCreatedTables)
                    creatingTables++;
            }*/
            sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_ROOM_TYPE_DATA, pars));
        }

        public void backToLobby(){
            sfs.removeAllEventListeners();
            state = ClientState.UserInfoGotten;
            sfs.addEventListener(SFSEvent.CONNECTION_LOST, lobbyEventListener);
            sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, lobbyEventListener);
        }

    }
}
