package vn.maiatech.king7.simulate;

/**
 * Created by delta on 12/27/2015.
 */
public class BotGenerator {
    private static BotGenerator ourInstance = new BotGenerator();

    public static BotGenerator getInstance() {
        return ourInstance;
    }

    private static final String DEFAULT_PASSWORD  = "96E79218965EB72C92A549DD5A330112"; // hash of '111111'

    private BotGenerator() {
    }

    public BotLoginInfo generate(int idx){
        //return new BotLoginInfo("__bot" + String.format("%05d", idx), DEFAULT_PASSWORD);
        return new BotLoginInfo(String.format(Config.getString("botgen.pattern"), idx), DEFAULT_PASSWORD);
    }

    public BotLoginInfo create(String username, String password){
        return new BotLoginInfo(username, password);
    }


    public class BotLoginInfo{
        public final String userName;
        public final String password;

        public BotLoginInfo(String userName, String password){
            this.userName = userName;
            this.password = password;
        }
    }
}
