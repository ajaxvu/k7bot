package vn.maiatech.king7.simulate;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import sfs2x.client.SmartFox;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.Room;
import sfs2x.client.entities.User;
import sfs2x.client.requests.ExtensionRequest;
import sfs2x.client.requests.LoginRequest;
import sfs2x.client.util.ConfigData;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Created by delta on 3/15/2016.
 */
public class GameAutoPlayRunner extends RunnerBase {
    private final GameAutoPlayCmd autoPlayCmd;
    private final K7ZoneInfo initZone;
    private final Class<?> gamePlayClass;
    private final List<GameAutoClient> clients;
    private int checkedClientIdx;
    private ScheduledFuture<?> generationTask;
    private BotGenerator.BotLoginInfo userInfo = null; // Support for single execution

    public GameAutoPlayRunner(String managedId, GameAutoPlayCmd cmd, K7ZoneInfo initZone, final Class<?> gamePlayClass) {
        id = managedId;
        this.autoPlayCmd = cmd;
        this.gamePlayClass = gamePlayClass;
        clients = new LinkedList<>();
        this.initZone = initZone;
    }

    // This is for supporting direct execution
    public GameAutoPlayRunner(String managedId,
                              String gameType,
                              K7ZoneInfo initZone,
                              K7ZoneInfo gameZoneInfo,
                              BotGenerator.BotLoginInfo userInfo,
                              Boolean willCreateTable,
                              final Class<?> gamePlayClass) {
        id = managedId;
        this.autoPlayCmd = new GameAutoPlayCmd();
        this.autoPlayCmd.gameType = gameType;
        this.autoPlayCmd.gameZoneInfo = gameZoneInfo;
        this.autoPlayCmd.maxPlayersInGame = 9;
        this.autoPlayCmd.players = 9;
        this.autoPlayCmd.toBeCreatedTables = willCreateTable ? 1 : 0;

        this.userInfo = userInfo;

        this.gamePlayClass = gamePlayClass;
        clients = new LinkedList<>();
        this.initZone = initZone;
    }


    protected void run() {
        // Bot assignment/arrangement
        int createdTables = 0;
        int humanWaitingTables = 0;
        for (int i = assignedUserIdxFrom; i <= assignedUserIdxTo; i++) {
            boolean isTableOwner = false;
            if (createdTables < autoPlayCmd.toBeCreatedTables) {
                isTableOwner = true;
                createdTables++;
            }

            BotGenerator.BotLoginInfo loginInfo = this.userInfo == null ? BotGenerator.getInstance().generate(i) : this.userInfo;
            GameAutoClient client = new GameAutoClient(loginInfo.userName, loginInfo.password,
                    autoPlayCmd.gameZoneInfo, autoPlayCmd.gameType, gamePlayClass, isTableOwner);
            clients.add(client);

            checkedClientIdx = 0;
            generationTask = new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            GameAutoClient client = clients.get(checkedClientIdx);

                            checkedClientIdx++;
                            if (checkedClientIdx >= clients.size())
                                checkedClientIdx = 0;

                            if (client.getState() == ClientState.NotConnected)
                                client.connect();
                            else if (client.getState() == ClientState.TableGroupsGotten)
                                client.createOrJoinRoom();
                        }
                    }
                    , 0, 5000, TimeUnit.MILLISECONDS);
        }
    }

    class GameAutoClient extends ClientBase {
        private final K7ZoneInfo gameZoneInfo;
        private String selectedRoomGroupId; //Note: this is the bad design of the game extension !

        public GameAutoClient(String userName, String password, K7ZoneInfo gameZoneInfo,
                              String gameType, Class<?> gamePlayClass, boolean isTableOwner) {
            super(userName, password, gameType, gamePlayClass, isTableOwner);
            this.gameZoneInfo = gameZoneInfo;
        }

        // Connect to init zone
        public void connect() {
            if (sfs != null)
                sfs.removeAllEventListeners();

            sfs = new SmartFox();
            cfg = new ConfigData();
            cfg.setHost(initZone.host);
            cfg.setPort(initZone.port);
            cfg.setZone(initZone.zoneName);
            cfg.setUseBBox(false);

            IEventListener evtListener = new InitZoneEvtHandler();

            sfs.addEventListener(SFSEvent.CONNECTION, evtListener);
            sfs.addEventListener(SFSEvent.CONNECTION_LOST, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN_ERROR, evtListener);
            sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);

            sfs.connect(cfg);

            state = ClientState.InitZoneConnecting;
        }

        public void connectToGameZone() {
            if (sfs != null)
                sfs.removeAllEventListeners();

            sfs = new SmartFox();
            cfg = new ConfigData();
            cfg.setHost(gameZoneInfo.host);
            cfg.setPort(gameZoneInfo.port);
            cfg.setZone(gameZoneInfo.zoneName);
            cfg.setUseBBox(false);

            IEventListener evtListener = new GameLobbyEvtHandler();

            lobbyEventListener = evtListener;

            sfs.addEventListener(SFSEvent.CONNECTION, evtListener);
            sfs.addEventListener(SFSEvent.CONNECTION_LOST, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN, evtListener);
            sfs.addEventListener(SFSEvent.LOGIN_ERROR, evtListener);
            sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, evtListener);

            sfs.connect(cfg);

            state = ClientState.GameZoneConnecting;
        }

        private class InitZoneEvtHandler implements IEventListener {
            @Override
            public void dispatch(BaseEvent evt) throws SFSException {
                String type = evt.getType();
                Map<String, Object> params = evt.getArguments();
                if (type.equals(SFSEvent.CONNECTION)) {
                    boolean success = (Boolean) params.get("success");
                    if (success) {
                        state = ClientState.InitZoneConnected;
                        ISFSObject pars = SFSObject.newInstance();
                        pars.putUtfString("type", "login");
                        pars.putUtfString("pass", password);
                        pars.putUtfString("partnerId", "K7");
                        sfs.send(new LoginRequest(userName, password, initZone.zoneName, pars));
                        state = ClientState.InitZoneLoggingIn;
                    } else {
                        state = ClientState.NotConnected;
                        System.err.println(String.format("%s: connection to init zone %s failure", userName, initZone.zoneName));
                    }
                } else if (type.equals(SFSEvent.LOGIN)) {
                    state = ClientState.InitZoneLoggedIn;
                    System.out.println(userName + ": login to zone " + initZone.zoneName + " success");
                    ISFSObject pms = (ISFSObject) params.get("data");
                    String token = pms.getUtfString("token");
                    //System.out.println("Token " + token);
                    userToken = token;
                    userId = pms.getUtfString("userID");
                    //requestUserInfo();  // TODO: will remove when try a better solution for game extension
                    // Switch to game zone
                    connectToGameZone();
                } else if (type.equals(SFSEvent.CONNECTION_LOST)) {
                    state = ClientState.NotConnected;
                    System.out.println(userName + ": connection to zone " + initZone.zoneName + " lost");
                } else if (type.equals(SFSEvent.LOGIN_ERROR)) {
                    state = ClientState.InitZoneConnected;
                    System.err.println(userName + ": login to zone " + initZone.zoneName + " error: " +
                            (String) params.get("errorMessage"));
                } else if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                    String cmd = (String) params.get("cmd");
                    SFSObject rspInfo = (SFSObject) params.get("params");
                    System.out.println(userName + ": received extension response from " +
                            initZone.zoneName + ". Command: " + cmd);
                    if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_USER_INFO)) {
                        if (rspInfo.getBool("status")) {
                            sfs.removeAllEventListeners();

                        } else {
                            System.err.println(userName + ": get user info in zone " + initZone.zoneName
                                    + " error: " + rspInfo.getUtfString("message"));
                        }
                    }
                }
            }
        }

        private class GameLobbyEvtHandler implements IEventListener {
            @Override
            public void dispatch(BaseEvent evt) throws SFSException {
                String type = evt.getType();
                Map<String, Object> params = evt.getArguments();
                if (type.equals(SFSEvent.CONNECTION)) {
                    boolean success = (Boolean) params.get("success");
                    if (success) {
                        state = ClientState.GameZoneConnected;
                        ISFSObject pars = SFSObject.newInstance();
                        //pars.putUtfString("type", "login");
                        pars.putUtfString("pass", password);
                        sfs.send(new LoginRequest(userName, password));
                        state = ClientState.GameZoneLoggingIn;
                    } else {
                        state = ClientState.NotConnected;
                        System.err.println(String.format("%s: connection to game zone %s failure", userName, gameZoneInfo.zoneName));
                    }
                } else if (type.equals(SFSEvent.LOGIN)) {
                    state = ClientState.GameZoneLoggedIn;
                    System.out.println(userName + ": login to game zone " + gameZoneInfo.zoneName + " success");
                    //ISFSObject pms = (ISFSObject) params.get("data");
                    //String token = pms.getUtfString("token");
                    //System.out.println("Token " + token);
                    //userToken = token;
                    //userId = pms.getUtfString("userID");
                    requestUserInfo();  // TODO: will remove when try a better solution for game extension

                } else if (type.equals(SFSEvent.CONNECTION_LOST)) {
                    state = ClientState.NotConnected;
                    System.out.println(userName + ": connection to game zone " + gameZoneInfo.zoneName + " lost");
                } else if (type.equals(SFSEvent.LOGIN_ERROR)) {
                    state = ClientState.GameZoneConnected;
                    System.out.println(userName + ": login to game zone " + gameZoneInfo.zoneName + " error: " +
                            (String) params.get("errorMessage"));
                } else if (type.equals(SFSEvent.EXTENSION_RESPONSE)) {
                    processGameExtResponse((String) params.get("cmd"), (SFSObject) params.get("params"));
                }
            }

            private void processGameExtResponse(String cmd, ISFSObject pms) {
                if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_USER_INFO)) {
                    state = ClientState.UserInfoGotten;
                    // Send command to get information about rooms, tables,...
                    requestRoomGroupsInfo(autoPlayCmd.gameZoneInfo.zoneName);
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_GET_ROOM_TYPE_DATA)) {
                    if (pms.getBool("status")) {
                        state = ClientState.TableGroupsGotten;
                        roomGroupIds = pms.getUtfStringArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_TYPE_ID);
                        createOrJoinRoom();
                    } else {
                        System.err.println(String.format(userName + ": get room data error: %s", pms.getUtfString("message")));
                    }

                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_CREATE_TABLE_PREPARE)) {
                    if (pms.getBool("status")) {
                        onReceivedTableGroupInfo(pms);
                    } else {
                        state = ClientState.TableGroupsGotten;
                        System.err.println(String.format(userName + ": table creating preparation error: %s",
                                pms.getUtfString("message")));
                    }
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_CREATE_TABLE)) {
                    if (pms.getBool("status")) {
                        int tableId = pms.getInt("roomID");
                        System.out.println(String.format("%s created table %d", userName, tableId));
                        requestJoinGame(autoPlayCmd.gameZoneInfo.zoneName, tableId, "KMS"); // TODO: hard code KMS, change later
                    } else {
                        state = ClientState.TableGroupsGotten;
                        System.err.println(String.format("%s: table creating error: %s", userName, pms.getUtfString("message")));
                    }
                } else if (cmd.equalsIgnoreCase(K7Def.SFS_CMD_USER_JOIN_ROOM)) {
                    if (pms.getBool("status")) {
                        if (pms.containsKey("islobby") && pms.getBool("islobby")) {
                            System.out.println(userName + ": joined to lobby room success");
                        } else {
                            System.out.println(String.format("%s: joined to game table %d success",
                                    userName, sfs.getJoinedRooms().get(0).getId()));
                            enterGame();
                        }
                    } else {
                        state = ClientState.TableGroupsGotten;
                        System.err.println(String.format(userName + ": join game error: %s", pms.getUtfString("message")));
                    }
                }
            }


            private void onReceivedTableGroupInfo(ISFSObject pms) {
                String tableDefaultName = pms.getUtfString(K7Def.SFS_PARAM_TABLE_LIST_ROOM_NAME);
                int maxUser = pms.getInt(K7Def.SFS_PARAM_TABLE_LIST_ROOM_MAX_USER);
                Collection<String> tableExtKeys = pms.getUtfStringArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_EXT_KEY);
                List<Double> tableBets = new ArrayList(pms.getDoubleArray(K7Def.SFS_PARAM_TABLE_LIST_ROOM_MONEY_CASH));

                ISFSObject pars = SFSObject.newInstance();
                pars.putUtfString("roomname", tableDefaultName + "_" + userName);
                pars.putUtfString("groupid", selectedRoomGroupId);
                pars.putDouble("roomcash", tableBets.size() > 0 ? tableBets.get(0) : 0); // Always choose the first defined bet
                pars.putInt("maxuser", maxUser);
                pars.putUtfString("zonename", autoPlayCmd.gameZoneInfo.zoneName);
                pars.putUtfString("password", "");
                pars.putUtfString ("currencyCode", "KMS");  // KMS hard-code. May change later

                if (tableExtKeys.size() > 0) {
                    Collection<Boolean> arrExtVal = Arrays.asList(new Boolean[]{true, false});  // TODO: may remove the hard code
                    pars.putBoolArray("extval", arrExtVal);
                    pars.putUtfStringArray("extkey", tableExtKeys);
                }

                sfs.send(new ExtensionRequest(K7Def.SFS_CMD_CREATE_TABLE, pars));
            }
        }

        // TODO: may change here
        private int getNumberOfBotsInRoom(Room room) {
            int cnt = 0;
            for (User u : room.getUserList()) {
                if (u.getName().startsWith("__bot"))
                    cnt++;
            }
            return cnt;
        }

        public void createOrJoinRoom() {
            if (isTableOwner && state == ClientState.TableGroupsGotten) {
                // Select a room group to create a table in
                int selectedIdx = 3; //Hard-code to g4  //ThreadLocalRandom.current().nextInt(0, roomGroupIds.size() - 1);
                List<String> lstRoomGroupIds = new ArrayList(roomGroupIds);
                selectedRoomGroupId = lstRoomGroupIds.get(selectedIdx);
                ISFSObject pars = SFSObject.newInstance();
                pars.putUtfString("zonename", autoPlayCmd.gameZoneInfo.zoneName);
                pars.putUtfString("groupid", selectedRoomGroupId);
                //pars.putUtfString("token", userToken);
                pars.putUtfString("currencyCode", "KMS");   // TODO: hard-code Shilling. May change later
                state = ClientState.TableCreating;
                sfs.send(new ExtensionRequest(K7Def.SFS_CMD_CREATE_TABLE_PREPARE, pars));
            } else {
                List<Room> tables = sfs.getRoomList();
                for (Room table : tables) {
                    if (table.isGame() && table.getUserCount() > 0 && getNumberOfBotsInRoom(table) <= autoPlayCmd.maxPlayersInGame - 2) {
                        // Try to join a game table
                        requestJoinGame(autoPlayCmd.gameZoneInfo.zoneName, table.getId(), "KMS");   // TODO: hard code CCY
                        break;
                    }
                }
            }
        }
    }

    public void doStop() {

    }
}
