package vn.maiatech.king7.simulate;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import sfs2x.client.SmartFox;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.requests.ExtensionRequest;
import sfs2x.client.requests.LeaveRoomRequest;
import sfs2x.client.util.ConfigData;

import java.util.Collection;

/**
 * Created by delta on 3/16/2016.
 */

public class ClientBase{
    protected SmartFox sfs;
    protected ConfigData cfg;
    protected final String userName;
    protected final String password;
    protected String userToken;
    protected String userId;
    protected final String gameType;
    protected final Class<?> gamePlayClass;
    protected ClientState state;
    protected boolean isTableOwner;
    protected IEventListener lobbyEventListener;
    protected Collection<String> roomGroupIds;

    public ClientBase(String userName, String password, String gameType, Class<?> gamePlayClass, boolean isTableOwner) {
        this.sfs = null;
        this.cfg = null;
        this.userName = userName;
        this.password = password;
        this.gameType = gameType;
        this.gamePlayClass = gamePlayClass;
        state = ClientState.NotConnected;
        this.isTableOwner = isTableOwner;
        this.roomGroupIds = null;
    }

    public final ClientState getState() {
        return state;
    }

    protected final void requestUserInfo() {
        ISFSObject pars = SFSObject.newInstance();
        if(userToken != null)
            pars.putUtfString("token", userToken);
        if(userId != null)
            pars.putUtfString("userID", userId);
        sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_USER_INFO, pars));
    }

    protected final void requestRoomGroupsInfo(String gameZoneName){
        ISFSObject pars = SFSObject.newInstance();
        pars.putUtfString("zonename", gameZoneName);
        sfs.send(new ExtensionRequest(K7Def.SFS_CMD_GET_ROOM_TYPE_DATA, pars));
    }

    protected final void requestJoinGame(String gameZoneName, int tableId, String selectedCCY) {
        state = ClientState.GameJoining;
        ISFSObject pars = SFSObject.newInstance();
        pars.putUtfString("zonename", gameZoneName);
        pars.putInt("roomid", tableId);
        pars.putUtfString("password", password);
        pars.putUtfString("userID", userId);
        pars.putUtfString("currencyCode", selectedCCY);
        System.out.println(String.format("%s: Try to join table: %d", userName, tableId));
        sfs.send(new ExtensionRequest(K7Def.SFS_CMD_USER_JOIN_ROOM, pars));
    }

    protected final void enterGame(){
        // Create game client
        try {
            GamePlayBase gc = (GamePlayBase)gamePlayClass.newInstance();
            SessionData sd = new SessionData();
            sd.userName = userName;
            sd.password = password;
            sd.token = userToken;
            //sd.cash = userCash;
            sd.gameType = gameType;
            sd.isRoomOwner = isTableOwner;
            state = ClientState.GameJoint;
            gc.enter(sfs, sd, this);
        } catch (Exception e) {
            System.err.println(String.format("%s: error when creating game client %s: %s",
                    userName, gamePlayClass.toString(), e.getMessage()));
            // Leave room
            sfs.send(new LeaveRoomRequest());
            state = ClientState.TableGroupsGotten;
        }
    }

    public void backToLobby(){
        sfs.removeAllEventListeners();
        state = ClientState.TableGroupsGotten;
        sfs.addEventListener(SFSEvent.CONNECTION_LOST, lobbyEventListener);
        sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, lobbyEventListener);
    }

    public final void disconnect() {
        if (sfs != null && sfs.isConnected()) {
            state = ClientState.NotConnected;
            sfs.disconnect();
        }
    }
}
