package vn.maiatech.king7.simulate;

/**
 * Created by delta on 12/24/2015.
 */
public class GameStressTestCmd {
    public String gameType;
    public int maxPlayersInGame;
    public K7ZoneInfo entranceServerInfo;
    public int ccu;
    public int toBeCreatedRooms;
    public int spectators = 0;
}
