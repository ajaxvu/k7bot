package vn.maiatech.king7.simulate;

/**
 * Created by delta on 1/4/2016.
 */
public abstract class RunnerBase {
    protected String id;
    protected int assignedUserIdxFrom;
    protected int assignedUserIdxTo;
    protected boolean isRunning = false;

    protected abstract void run();

    protected abstract void doStop();

    public void run(int assignedUserIdxFrom, int assignedUserIdxTo) {
        this.assignedUserIdxFrom = assignedUserIdxFrom;
        this.assignedUserIdxTo = assignedUserIdxTo;
        isRunning = true;
        run();
    }

    public void stop() {
        isRunning = false;
        doStop();
    }

    public String getManagedId() {
        return id;
    }

    public int getAssignedUserIdxFrom() {
        return assignedUserIdxFrom;
    }

    public int getAssignedUserIdxTo() {
        return assignedUserIdxTo;
    }
}
