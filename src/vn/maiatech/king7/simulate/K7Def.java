package vn.maiatech.king7.simulate;

/**
 * Created by delta on 1/5/2016.
 */
public class K7Def {
    public static String SFS_CMD_GET_GAME_TYPE_DATA = "GET_GAME_TYPES";
    public static String SFS_CMD_GET_ROOM_TYPE_DATA = "GET_ROOM_TYPES";

    public static String SFS_PARAM_TABLE_LIST_ROOM_TYPE_ID = "roomTypeID";
    public static String SFS_PARAM_TABLE_LIST_ROOM_TYPE_NAME = "roomTypeName";

    public static String SFS_CMD_CREATE_TABLE_PREPARE = "CREATE_TABLE_PREPARE";
    public static String SFS_CMD_CREATE_TABLE = "CREATE_TABLE";
    public static String SFS_CMD_GET_USER_INFO = "GET_USER_INFO";
    public static String SFS_CMD_USER_JOIN_ROOM = "USER_JOIN_ROOM";
    public static String SFS_CMD_USER_JOIN_ROOM_LOBBY = "USER_JOIN_ROOM_LOBBY";

    public static String SFS_PARAM_TABLE_LIST_ROOM_MONEY_CASH = "roomMoneyCash";
    public static String SFS_PARAM_TABLE_LIST_ROOM_NAME = "roomName";
    public static String SFS_PARAM_TABLE_LIST_ROOM_MAX_USER = "roomMaxUser";
    public static String SFS_PARAM_TABLE_LIST_ROOM_EXT_NAME = "roomParaExtName";
    public static String SFS_PARAM_TABLE_LIST_ROOM_EXT_KEY = "roomParaExtKey";
}
