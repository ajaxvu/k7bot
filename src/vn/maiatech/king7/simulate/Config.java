package vn.maiatech.king7.simulate;

import java.util.Properties;

/**
 * Created by delta on 1/8/2016.
 */
public class Config {
    private static Properties props = null;

    public static void load(Properties props) {
        if(Config.props == null)
            Config.props = props;
    }

    public static String getString(String key){
        if(props != null)
            return props.getProperty(key);
        else
            throw new NullPointerException("Config not loaded yet");
    }

    public static int getInt(String key){
        if(props != null) {
            String value = props.getProperty(key);
            try {
                return Integer.parseInt(value);
            }
            catch(Exception e){
                throw new ClassCastException("Exception in parsing config " + key + ": " + e.getMessage());
            }

        }
        else
            throw new NullPointerException("Config not loaded yet");
    }
}
