import vn.maiatech.king7.simulate.*;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by delta on 12/24/2015.
 */
public class GameBot {

    public static void main(String[] args) throws Exception {
       /* String defaultCfg = args.length > 0 ? args[0] : "config.properties";

        Properties props = new Properties();
        props.load(new FileInputStream(defaultCfg));
        Config.load(props);*/

        //new MessageListener();

        // Temporary logic for direct execution !!!!
        // Run: jar K7GameBot.jar <gameType> <username> <password> [createTableOrNot]
        // VD: jar K7GameBot.jar King7_XocDia tuanvd 12345 1  (bot tao ban)
        //     jar K7GameBot.jar King7_XocDia tuanvd 12345    (bot ko tao ban)

        String gameType = args[0];
        String userName = args[1];
        String password = Util.calcMd5(args[2]);
        Boolean createTable = args.length > 3 ? (args[3].equals("1") ? true : false) : false;

        String defaultCfg = args.length > 4 ? args[4] : "config.properties";
        Properties props = new Properties();
        props.load(new FileInputStream(defaultCfg));
        Config.load(props);


        RunManager runManager = RunManager.getInstance();
        // Set index range manually: only one client!
        runManager.initialize(1, 1);
        String clientClassName = Config.getString(gameType + ".clientClassName");

        // Setting server info
        K7ZoneInfo initZoneInfo = new K7ZoneInfo();
        initZoneInfo.host = Config.getString("entrance.host");
        initZoneInfo.port = Config.getInt("entrance.port");
        initZoneInfo.zoneName =  Config.getString("entrance.zone");

        K7ZoneInfo gameZoneInfo = new K7ZoneInfo();
        gameZoneInfo.host = Config.getString("gameserver.host");
        gameZoneInfo.port = Config.getInt("gameserver.port");
        gameZoneInfo.zoneName = gameType;

        BotGenerator.BotLoginInfo userInfo = BotGenerator.getInstance().create(userName, password);

        if (clientClassName != null) {
            GameAutoPlayRunner runner = new GameAutoPlayRunner(
                    "12345", gameType, initZoneInfo, gameZoneInfo, userInfo, createTable, Class.forName(clientClassName)
            );
            runManager.addRunner(runner, 1);
            while (true) {
            }
        } else
            System.err.println(String.format("Client of game type %s undefined", gameType));

    }
}
